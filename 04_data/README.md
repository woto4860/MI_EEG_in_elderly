# Data  
In this directory where all raw data files and data outputs are saved.<br>
Subfolder are created by <b>MatLab</b> scripts from folder 05_scritps.

## Versioning
<i>Version 1.0 // 21.08.18</i>

## Author
Developed by Marieke Daeglau, University of Oldenburg <br>
Adopted by Julius Welzel, University of Oldenburg

