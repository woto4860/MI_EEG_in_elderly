# Exploring time – Motor imagery EEG Features in elderly
This directory contains library for overleaf(LaTeX) code to generate pdf file <br>
* main - contains code to generate overall document
* sections - contain subdocument where text is written
* images - directorsy to storwe images, used in main file
* bib - Added bibliography to cite papers :D

## Versioning
<i>Version 1.0 // 10.01.19</i>
<i>Version 1.1 // 06.04.19 // Some sections updated</i>

## Author
Developed by Julius Welzel, University of Oldenburg <br>
Supervised by Mareike Daeglau & Cornelia Kranczioch, University of Oldenburg 

  
