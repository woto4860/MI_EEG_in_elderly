%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%                  EMG data analysis
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% EMG main
% Author: Mareike Daeglau // University of Oldenburg, 2018
% edited: 11.12.2018 // Julius Welzel

PATHIN = [MAIN '04_Data\ana03_emg\'];
PATHOUT_SAN = [PATHIN 'emg_plots\'];

if ~exist(PATHOUT_SAN)
  mkdir(PATHOUT_SAN);
end

list = dir(fullfile([MAIN '04_Data\ana02_clean_ep\' '*clean.set']));
SUBJ = extractBefore({list.name},'_');

load ([PATHIN 'emg_all.mat']);

%reasoning and visualization go here: idea: check if movingstd exceeds std
%of trial of active hand (here only right) or resting hand (here only
%left). 
%case 1: movingstd exceeds std of trial in the active hand: reject trial
%case 2: movingstd exceeds std of trial in the resting hand but not in the
%active hand: sum both EMG streams and check again, if movingstd exceeds
%std of trial in the combined stream: reject, if not: keep

%note: you must know which channel is for which hand, because this is not
%the case here, we check all streams

load ([MAIN '04_Data\ana01_ICAw\cfg.mat']);

cfg.EMG.windN_movSD = 125;
cfg.EMG.thresh_BP = 900; 
win_size = cfg.EMG.windN_movSD;
thresh_BP = cfg.EMG.thresh_BP;


%% check different streams, only for visualization purposes
%{

for sub = 1:size(emgdata,1)
    
    for t = 1:16 %8 ME & 8 MI trials
        figure; hold on;
        
        % Right hand
        subplot(3,1,1); hold on;
        satt2 = movstd(emgdata{sub,t}(1,:),win_size);
        satt = std(emgdata{sub,t}(1,:));
        plot(emgdata{sub,t}(1,:))
        hline(satt)
        plot(satt2)
        c = find(satt2 > satt);
        %axis([0 5500 -20 20]);
        if ~isempty(c)
            vline(c(1))
        end
        title 'Right hand'
        
        %Left hand
        subplot(3,1,2); hold on;
        satt2 = movstd(emgdata{sub,t}(2,:),win_size);
        satt = std(emgdata{sub,t}(2,:));
        plot(emgdata{sub,t}(2,:))
        hline(satt)
        plot(satt2)
        c = find(satt2 > satt);
        %axis([0 5500 -20 20]);
        if ~isempty(c)
            vline(c(1))
        end 
        title 'Left hand'
        
        %Summed hand
        summed = emgdata{sub,t}(1,:)+emgdata{sub,t}(2,:);
        subplot(3,1,3); hold on;
        satt2 = movstd(summed,win_size);
        satt = std(summed);
        plot(summed)
        hline(satt)
        plot(satt2)
        c = find(satt2 >satt);
        %axis([0 5500 -20 20]);
        if ~isempty(c)
            vline(c(1))
        end
        title 'Summed hands'
        print('-dpng', [PATHOUT_SAN,SUBJ{sub},'_trial_',num2str(t),'.png']);  % raster image
        close;
    end
end
%}

%% main analysis goes here
%define variables
n_sub = size(emgdata,1);
n_trials = 96;
f_std = linspace(1,3,9);

c_a = 1;

for std_try = 1:length(f_std)

    c1_all = zeros(n_sub,n_trials);
    c2_all = zeros(n_sub,n_trials);
    c1 = zeros(n_sub,n_trials);
    c2 = zeros(n_sub,n_trials);
    c = [];
    cca = [];
    % only interesting for the MI trials
    for sub = 1:size(emgdata,1)

        for t = 1:n_trials

            satt2 = movstd(emgdata{sub,t}(1,:),win_size); %125 sample windows, you can play around with this parameter and check in the loop above
            satt = std(emgdata{sub,t}(1,:)); %STD for right(1) channel
            c = find(satt2 >(f_std(std_try)*satt)); % first exceed thresh STD

            %including button press rigth hand
            if ~isempty(c) 
                c1_all(sub,t) = c(1); % BP sample
            end


            %indices without button press

            % right hand
            if ~isempty(c) && c(1) < size(emgdata{sub,t}(1,:),2)-thresh_BP %<- threshold to account for the button press (750 ms), check if index is lower so that movement is not derived from button press
                c1(sub,t) = c(1); % position where movavg exceeds std
            end

            %left hand // only when no movement in rigth hand
            if isempty(c)
                satt3 = movstd(emgdata{sub,t}(2,:),win_size);
                satt4 = std(emgdata{sub,t}(2,:));
                cca = find(satt3 >(satt4));

                if ~isempty(cca) 
                    c2_all(sub,t) = cca(1); % left hand only
                end

                if ~isempty(cca) && cca(1) < size(emgdata{sub,t}(2,:),2)-thresh_BP %left hand without BP
                    c2(sub,t) = cca(1);
                end
            end

            %summed hands
            summed = emgdata{sub,t}(1,:)+emgdata{sub,t}(2,:);
            satt2 = movstd(summed,win_size);
            satt = std(summed);
            c = find(satt2 >f_std(std_try)*satt);

            if ~isempty(c) && c(1) < size(emgdata{sub,t}(2,:),2)-thresh_BP
                c1(sub,t) = c(1); %-> stores all invalid trials
            end 

        end

    end

    c1(c1==0) = NaN;
    c2(c2==0) = NaN;
    c1_all(c1_all==0) = NaN;
    c2_all(c2_all==0) = NaN;

    %% EMG validity plots without invalid ME trials (discard marble drop)

   EMG_overall = nan(sub,n_trials);

    for sub = 1:size(emgdata,1)

        for t = 1:n_trials
           EMG_overall(sub,t)  = emg_check(sub).trials_dur(t);
           if isnan(c1(sub,t))  && ismember(t,cfg.blck_idx{1,:}) | ismember(t,cfg.blck_idx{4,:}) % movement blocks
               EMG_overall(sub,t)  = NaN;
           elseif ~isnan(c1(sub,t)) | emg_check(sub).trials_valid(t) == 0 && ismember(t,cfg.blck_idx{2,:}) | ismember(t,cfg.blck_idx{3,:}) % imagery blocks
               EMG_overall(sub,t)  = NaN;
           end
        end


    end

    %% EMG validity plots with invalid ME trials (Marble drop)
%{        
    EMG_overall = nan(size(emgdata,1),n_trials);

    for sub = 1:size(emgdata,1)

        for t = 1:n_trials
           EMG_overall(sub,t)  = emg_check(sub).trials_dur(t);
           if isnan(c1(sub,t)) | emg_check(sub).trials_valid(t) == 0 && ismember(t,cfg.blck_idx{1,:}) | ismember(t,cfg.blck_idx{4,:}) % movement blocks
               EMG_overall(sub,t)  = NaN;
           elseif ~isnan(c1(sub,t)) | emg_check(sub).trials_valid(t) == 0 && ismember(t,cfg.blck_idx{2,:}) | ismember(t,cfg.blck_idx{3,:}) % imagery blocks
               EMG_overall(sub,t)  = NaN;
           end
        end


    end
%}
    %% Plots
    % Errorbars
    RTs = [nanmean(EMG_overall(:,cfg.blck_idx{1,:}),2) nanmean(EMG_overall(:,cfg.blck_idx{2,:}),2) nanmean(EMG_overall(:,cfg.blck_idx{3,:}),2) nanmean(EMG_overall(:,cfg.blck_idx{4,:}),2)];
    ACCs = 100*[NaN_acc(EMG_overall(:,cfg.blck_idx{1,:})) NaN_acc(EMG_overall(:,cfg.blck_idx{2,:})) NaN_acc(EMG_overall(:,cfg.blck_idx{3,:})) NaN_acc(EMG_overall(:,cfg.blck_idx{4,:}))];

    [meanRT semRT] = mean_SEM(RTs');
    [meanACC semACC] = mean_SEM(ACCs');

   
    M = figure;
    left_color = [1 0 0];
    right_color = [0 0 1];
    set(M,'defaultAxesColorOrder',[left_color; right_color]);
    yyaxis left
    errorbar(meanRT,semRT,'-s','color','r','MarkerFaceColor','red','MarkerSize',10)
    % ylim([1000 2000])
    ylabel 'RT[ms]'

    hold on
    yyaxis right
    errorbar(meanACC,semACC,'-s','color','b','MarkerFaceColor','blue','MarkerSize',10)
    ylabel 'Epochs clean [%]'

    xlim([0 size(RTs,2)+1])
    xlabel 'Blocks'
    xticks([1:4])
    xticklabels({'ME1','MI1','MI2','ME2'})
    title (['RTs OLD // Clean epochs // STD-Thresh: ' num2str(f_std(std_try))])'
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
    hline(70,'k')

    set(gcf, 'Position', [0 0 1200 800]);
    saveas(gcf,[PATHOUT_SAN,'MEAN_EMG','_std_',num2str(f_std(std_try)),'.png']);
    close;

    % Scatter
    Ind = figure;
    CT=cbrewer('qual', 'Paired', size(RTs,1));

    for n = 1:size(RTs,1)
        j = -0.1 + (0.1+0.1)*rand(1,1);
        plot([1:size(RTs,2)]+j,ACCs(n,:),'-s','Color',CT(n,:),'MarkerFaceColor',CT(n,:), 'MarkerEdgeColor','none','MarkerSize',10)
        hold on
    end

    ylabel 'Epochs clean [%]'
    xlim([0 size(RTs,2)+1])
    xlabel 'Blocks'
    xticks([1:4])
    xticklabels({'ME1','MI1','MI2','ME2'})
    title (['Clean epochs // STD-Thresh: ' num2str(f_std(std_try))])
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
    legend ([SUBJ])
    hline(70,'k')

    set(gcf, 'Position', [0 0 1200 800]);
    saveas(gcf,[PATHOUT_SAN,'MEAN_EMG_scatter','_std_',num2str(f_std(std_try)),'.png']);
    close;
    

    acc_overall(c_a) = mean(mean(ACCs));
    c_a = c_a+1;
end


figure
plot(acc_overall,'--s')
xlim([0 length(acc_overall)+1])
xticks([1:length(acc_overall)])
xticklabels({[f_std]})
xlabel 'STD threshold'
ylabel 'Epochs clean [%]'
set(findall(gcf,'-property','FontSize'),'FontSize',18)
set(gcf, 'Position', [0 0 1200 800]);
saveas(gcf,[PATHOUT_SAN,'Overvier threshold','_std_','.png']);
close;




