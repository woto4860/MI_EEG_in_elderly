%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%               Process of behavioural data from table
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Data: juw_el_table (Mareike Daeglau, University of Oldenburg)
% Author: Julius Welzel

% define paths
PATHIN = [MAIN '04_Data\ana11_behavioural\'];
PATHIN_RTs = [MAIN '04_Data\ana04_erd\'];
PATHIN_EMG = [path_data 'ana03_emg\'];

PATHOUT= [MAIN '04_Data\ana11_behavioural\'];

if ~exist(PATHOUT)
  mkdir(PATHOUT);
end

load([PATHIN 'behavioural.mat']);
load([PATHIN_RTs 'cfg.mat']);

list = dir(fullfile([path_data 'ana02_clean\*clean*.set']));
SUBJ = extractBefore({list.name},'_');

%% Scatter Age vs. MoCa

age = table2array(behavioural(:,3));
MoCa = table2array(behavioural(:,4));
sex = table2array(behavioural(:,2));
[am as] = mean_SEM(age');
[mm ms] = mean_SEM(MoCa');
[sm ss] = mean_SEM(sex');

b = figure;
[unique_groups, ~, group_idx] = unique(sex);
pointsize = 150;
scatter(age,MoCa, pointsize, group_idx,'s','filled');
cmap = cbrewer('seq', 'Greens',6);    %or build a custom color map
colormap( [0.5 0.5 0.5;cmap(4,:)] );
legend('female','male');
ylim([23 30])
ylabel 'MoCA score'
xlim([50 85])
xlabel 'Age [years]'
legend boxoff
save_fig(gcf,PATHOUT,'scatter_moca_age','fontsize',25,'figsize',[0 0 20 20])

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%                           Reaction times
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% extract trial relevant markers // 1 = Start, 0 = Stimulus start, 2 = trial end
load ([PATHIN_RTs 'RTs.mat']);
load ([PATHIN_RTs 'cfg.mat']);
load ([PATHIN_EMG 'emg_all.mat']);
cfg.n_trials = 96;
SUBJ = str2double(SUBJ);


%% plot
old = (SUBJ>=80);
young = (SUBJ<80);
g_idx = ones(1,(length(RT)));
g_idx(old) = 0;


blocks = cell(1,cfg.n_trials);
blocks(cfg.blck_idx{1,1}) = {'EX1'};
blocks(cfg.blck_idx{2,1}) = {'MI1'};
blocks(cfg.blck_idx{3,1}) = {'MI2'};
blocks(cfg.blck_idx{4,1}) = {'EX2'};


for t = 1:length(RT)

    ex1(:,t) = RT(t).times(cfg.blck_idx{1,1});
    ex1(~emg_check(t).trials_valid(cfg.blck_idx{1,1}),t) = NaN;
    mi1(:,t) = RT(t).times(cfg.blck_idx{2,1});
    mi2(:,t) = RT(t).times(cfg.blck_idx{3,1});
    
    if length(RT(t).times)>100
        ex2(:,t) = RT(t).times(249:256);
        ex2(~emg_check(t).trials_valid(249:256),t) = NaN;

    else
        ex2(:,t) = RT(t).times(cfg.blck_idx{4,1});
        ex2(~emg_check(t).trials_valid(cfg.blck_idx{4,1}),t) = NaN;

    end
    
end

RT_all = [ex1;mi1 ;mi2 ;ex2];

%mean for each block
ex1_o = mean(nanmean(ex1(:,old)));
mi1_o = mean(nanmean(mi1(:,old)));
mi2_o = mean(nanmean(mi2(:,old)));
ex2_o = mean(nanmean(ex2(:,old)));

ex1_y = mean(nanmean(ex1(:,young)));
mi1_y = mean(nanmean(mi1(:,young)));
mi2_y = mean(nanmean(mi2(:,young)));
ex2_y = mean(nanmean(ex2(:,young)));

% ste for each block
ex1_o_se = ste(nanmean(ex1(:,old)));
mi1_o_se = ste(nanmean(mi1(:,old)));
mi2_o_se = ste(nanmean(mi2(:,old)));
ex2_o_se = ste(nanmean(ex2(:,old)));

ex1_y_se = ste(nanmean(ex1(:,young)));
mi1_y_se = ste(nanmean(mi1(:,young)));
mi2_y_se = ste(nanmean(mi2(:,young)));
ex2_y_se = ste(nanmean(ex2(:,young)));



all_t_o = [nanmean(ex1(:,old),2);nanmean(mi1(:,old),2);nanmean(mi2(:,old),2);nanmean(ex2(:,old),2)];
ste_t_o = [ste(ex1(:,old)'),ste(mi1(:,old)'),ste(mi2(:,old)'),ste(ex2(:,old)')];
err_b_o = [ste_t_o;-ste_t_o];


all_t_y = [nanmean(ex1(:,young),2);nanmean(mi1(:,young),2);nanmean(mi2(:,young),2);nanmean(ex2(:,young),2)];
ste_t_y = [ste(ex1(:,young)'),ste(mi1(:,young)'),ste(mi2(:,young)'),ste(ex2(:,young)')];
err_b_y = [ste_t_y;-ste_t_y];

x=(1:size(all_t_o,1));

%% shaded errorbar all trials
figure
shadedErrorBar(x,all_t_y,ste_t_y','lineprops','-r')
hold on
shadedErrorBar(x,all_t_o,ste_t_o','lineprops','-m')

ylabel('Mean Trial Times [ms]');
xlabel('Trials');
xlim([1 96]);

vline([8,48,88],'-k')

%set figure screensize and save
save_fig(gcf,PATHOUT,'RTs','FontSize',25,'figsize',[0 0 35 20])

%% shaded errorbar blocks
bz = 2+1;
b_o = 1:bz:cfg.num_block*bz;
b_y = [1+1:bz:cfg.num_block*bz]+1;
b_b = [1+2:bz:cfg.num_block*bz-1]+2;
b = 1:cfg.num_block

blck_time_y = [ex1_y mi1_y mi2_y ex2_y];
blck_time_o = [ex1_o mi1_o mi2_o ex2_o];

blck_time_y_se = [ex1_y_se mi1_y_se mi2_y_se ex2_y_se];
blck_time_o_se = [ex1_o_se mi1_o_se mi2_o_se ex2_o_se];

figure
t_y = shadedErrorBar(b,blck_time_y,blck_time_y_se,'lineprops',{'-','Color',c_young,'LineWidth',2})
hold on
e_y = errorbar(blck_time_y,blck_time_y_se,'-','Color',c_young,'LineWidth',2)
hold on
t_o = shadedErrorBar(b,blck_time_o,blck_time_o_se,'lineprops',{'-','Color',c_old,'LineWidth',2})
hold on
e_o = errorbar(blck_time_o,blck_time_o_se,'-','Color',c_old,'LineWidth',2)


t_o.edge(1).LineWidth = 2;
t_o.edge(2).LineWidth = 2;
t_y.edge(1).LineWidth = 2;
t_y.edge(2).LineWidth = 2;

xlim([0.8 4.2])
xticks (b);
xticklabels({'EX 1','MI 1','MI 2','EX 2'})
ylabel('[ms]');
xlabel('Blocks');

legend ([e_o,e_y],{'Old','Young'})
legend boxoff

%set figure screensize and save
save_fig(gcf,PATHOUT,'RTs_blck.png','fontsize',25,'figsize',[0 0 30 20])


%% Boxplots
bz = 2+1;
b_o = 1:bz:cfg.num_block*bz;
b_y = [1+1:bz:cfg.num_block*bz]+1;
b_b = [1+2:bz:cfg.num_block*bz]+2;

n_old = sum(old);
blck_lab(1:n_old) = {'EX1'};
blck_lab(n_old+1:n_old*2) = {'MI1'};
blck_lab(2*n_old+1:n_old*3) = {'MI2'};
blck_lab(3*n_old+1:n_old*4) = {'EX2'};


b_c = 1;
for b = 1:cfg.num_block  
    b_p(:,b_c) = nanmean(RT_all(cfg.blck_idx{b,1},young));
    b_c = b_c+1;
    b_p(:,b_c) = nanmean(RT_all(cfg.blck_idx{b,1},old))
    b_c = b_c+1;
end

bp = figure
boxplot(b_p,'Colors',c_o)
h = findobj(gca,'Tag','Box');
for j=1:2:length(h)-1
patch(get(h(j),'XData'),get(h(j),'YData'),c_y,'FaceAlpha',.5);
patch(get(h(j+1),'XData'),get(h(j+1),'YData'),c_o,'FaceAlpha',.5);

end




b_mi_o = struct('y', [nanmean(ex1(:,old))  nanmean(mi1(:,old))  nanmean(mi2(:,old))  nanmean(ex2(:,old))],'x',blck_lab,'name', 'Old','marker', struct('color',c_o),'type', 'box');
b_mi_y = struct('y', [nanmean(ex1(:,young))  nanmean(mi1(:,young))  nanmean(mi2(:,young))  nanmean(ex2(:,young))],'x',blck_lab,'name','Young','marker', struct('color',c_y),'type', 'box');

EX1_y = struct('y', nanmean(ex1(:,young)),'type', 'box');

MI1_o = struct('y', nanmean(mi1(:,old)),'type', 'box');
MI1_y = struct('y', nanmean(mi1(:,young)),'type', 'box');

MI2_o = struct('y', nanmean(mi2(:,old)),'type', 'box');
MI2_y = struct('y', nanmean(mi2(:,young)),'type', 'box');

EX2_o = struct('y', nanmean(ex2(:,old)),'type', 'box');
EX2_y = struct('y', nanmean(ex2(:,young)),'type', 'box');


data = {b_mi_y,b_mi_o};
layout = struct(...
    'yaxis', struct(...
      'title', 'Trial times [ms]', ...
      'zeroline', false), ...
    'boxmode', 'group');
response = plotly(data, struct('layout', layout, 'filename', 'box-grouped', 'fileopt', 'overwrite'));
plot_url = response.url

%%
%{
% color grouping data (number of cylinders) and select a subset of the data
g=gramm('x',repmat([1:cfg.n_trials],length(RT),1)','y',RT_all,'color',g_idx);
g.facet_grid([],blocks');
g.stat_boxplot();
g.set_names('column','Block','x','Trials','y','RT [ms]','color','Group');
g.draw();

%}
