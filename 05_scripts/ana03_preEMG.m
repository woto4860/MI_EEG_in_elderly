%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%               Process EMG data of elderly participants
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Data: juw_el_table (Mareike Daeglau, University of Oldenburg)
% Author: Julius Welzel

% define paths
PATHIN_EMG = [MAIN '04_Data\ana02_clean\'];
PATHOUT_EMG = [MAIN '04_Data\ana03_emg\'];

if ~exist(PATHOUT_EMG)
  mkdir(PATHOUT_EMG);
end 

% document potential problems with a subject
if exist([PATHOUT_EMG 'docError.mat']) == 2
    load([PATHOUT_EMG 'docError.mat']);
else
    docError = {};      
end

%call eeglab
% [ALLEEG, EEG, CURRENTSET, ALLCOM] = eeglab;


%% creating variables for preprocessing

load ([MAIN '04_Data\ana01_ICAw\cfg.mat']);

cfg.EMG.EEG_ELECS={'E65' 'E66'};
cfg.EMG.TRIGGER_start={'1'};
cfg.EMG.TRIGGER_stop = {'End'};
cfg.EMG.Cutoff_highpass = 25;

cfg.EMG.epochs.young = [cfg.blck_idx{1,1}(1): cfg.blck_idx{3,1}(end) 249:256];
cfg.EMG.epochs.old = [cfg.blck_idx{1,1}(1): cfg.blck_idx{4,1}(end)];


%% FIND DATASETS

% find all Datasets
list = dir(fullfile([PATHIN_EMG '*clean*.set']));
SUBJ = extractBefore({list.name},'_');

for sub = 1:length(SUBJ)
    
    if str2num(SUBJ{sub})<50;    
        EEG = pop_loadset('filename',[SUBJ{sub},'_clean_ICA.set'],'filepath',PATHIN_EMG);
    else
        EEG = pop_loadset('filename',[SUBJ{sub},'_clean.set'],'filepath',PATHIN_EMG);
    end

    EEG = eeg_checkset(EEG, 'eventconsistency' );
    %use 2 channels (omit EEG and others)
    EEG = pop_select( EEG,'channel',cfg.EMG.EEG_ELECS);
    %rename events
    EEG  = renameEvents_table(EEG);
    %rename redundant triggers
    EEG = triggerhand(EEG);
    %high-pass filter
    EEG = pop_firws(EEG, 'fcutoff', cfg.EMG.Cutoff_highpass, 'ftype', 'highpass', 'wtype', 'hamming', 'forder', 264); 
    
    emg_check(sub).ID = SUBJ{sub};
    
    count=1;
    for h = 1:size(EEG.event,2)
        
        if strcmp(EEG.event(h).type, '1')
            
            begin_epoch = EEG.event(h).latency;
            flag = true;
            
        elseif strcmp(EEG.event(h).type, 'End') && flag == true
                       
            last_epoch = EEG.event(h).latency;
            emgdata{sub,count}(:,:) = EEG.data(:,begin_epoch:last_epoch); 
            
            flag = false;
            
            emg_check(sub).trials_dur(count) = [last_epoch-begin_epoch];
            
            if strcmp(EEG.event(h+2).type,'valid')
                emg_check(sub).trials_valid(count) = 1;
            elseif strcmp(EEG.event(h+2).type,'invalid')
                emg_check(sub).trials_valid(count) = 0;
            end
            
            count = count+1;
                            
        end
       
        
    end
    
end


%%
cfg.EMG.srate = EEG.srate;
save([PATHOUT_EMG 'emg_all.mat'],'emgdata','emg_check');
save([PATHOUT_EMG 'cfg.mat'],'cfg');