%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%                       Explore pilot data
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% PP analysis for frequencyspace patterns in MI
% Data: mad_table (Mareike Daeglau, University of Oldenburg)
% Author: Julius Welzel

% define paths
PATHIN = path_data;
PATHOUT = [MAIN '04_Data\Pilot\ana01_ICAw\'];

if ~exist(PATHOUT)
  mkdir(PATHOUT);
end

%% DEFINE CONFIG VARIABLES

% define blocks
  cfg.tr = 8;
  cfg.mi = 40;
  cfg.num_block = 4;
  cfg.blck_idx = {1:8};
  for nb = 2:cfg.num_block
    if nb ~=4
      cfg.blck_idx{nb,:} = cfg.blck_idx{end}(end)+1:cfg.blck_idx{end}(end)+cfg.mi;
    elseif nb==4
      cfg.blck_idx{nb,:} = cfg.blck_idx{end}(end)+1:cfg.blck_idx{end}(end)+cfg.tr;
    end
  end
  
cfg.n_chan = [1:64];
cfg.channels = {'E01' 'E02' 'E03' 'E04' 'E05' 'E06' 'E07' 'E08' 'E09' 'E10' 'E11' 'E12' 'E13' 'E14' 'E15' 'E16' 'E17' 'E18' 'E19' 'E20' 'E21' 'E22' 'E23' 'E24' 'E25' 'E26' 'E27' 'E28' 'E29' 'E30' 'E31' 'E32' 'E33' 'E34' 'E35' 'E36' 'E37' 'E38' 'E39' 'E40' 'E41' 'E42' 'E43' 'E44' 'E45' 'E46' 'E47' 'E48' 'E49' 'E50' 'E51' 'E52' 'E53' 'E54' 'E55' 'E56' 'E57' 'E58' 'E59' 'E60' 'E61' 'E62' 'E63' 'E64'};

% resampling
cfg.resample = 250;

% Creating variables for ICA

cfg.ICA.LP= 40;
cfg.ICA.HP= 1;
cfg.ICA.prune = 3;
%% FIND DATASETS

% find all Datasets
list = dir(fullfile([PATHIN '*.xdf']));
SUBJ = extractBefore({list.name},'.');

% eeglab

%% LOAD DATASET
s = 10;

EEG = pop_loadxdf([PATHIN SUBJ{s} '.xdf'], 'streamtype', 'EEG', 'exclude_markerstreams', {});
EEG = pop_editset(EEG, 'setname',['pilot_' SUBJ{s}]);
EEG = eeg_checkset(EEG, 'eventconsistency' );

% clean markers (Mareike Daeglau)
EEG = renameEvents_table(EEG);
EEG.event = rmfield(EEG.event, 'del');

%use 64 channels (omit EMG and others)
EEG = pop_select( EEG,'channel',cfg.channels);
%add chanlocs
EEG=pop_chanedit(EEG, 'lookup',[MAIN '101_software\elec_64ch.elp']);
% resample
EEG = pop_resample( EEG,cfg.resample);

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%                       Check Dataset for consistency
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% OVERVIEW
% Start marker & counter
star = 0;
t = 0;
eins = 0;
r = 0;
op = 0;
ende = 0;
c = 1;

for e = 1:length(EEG.event)
    
     %count Eventmarker types
     if   ~isempty(strfind(EEG.event(e).type, '-Start-'))
        mark(c).type = EEG.event(e).type;
        mark(c).time = EEG.event(e).latency;
        star = star+1;
        
     elseif ~isempty(strfind(EEG.event(e).type, 'transp'))
        mark(c).type = EEG.event(e).type;
        mark(c).time = EEG.event(e).latency;
        t = t+1;
  
     elseif   strcmp(EEG.event(e).type, '1')
        mark(c).type = EEG.event(e).type;
        mark(c).time = EEG.event(e).latency;
        eins = eins+1;
                    
     elseif   ~isempty(strfind(EEG.event(e).type, 'Release'))
        mark(c).type = EEG.event(e).type;
        mark(c).time = EEG.event(e).latency;
        r = r+1;
                
     elseif   ~isempty(strfind(EEG.event(e).type, 'opaque'))
        mark(c).type = EEG.event(e).type;
        mark(c).time = EEG.event(e).latency;
        op = op+1;
                
     elseif   ~isempty(strfind(EEG.event(e).type, 'End'))
        mark(c).type = EEG.event(e).type;
        mark(c).time = EEG.event(e).latency;
        ende = ende+1;
     end
       
     c = c+1;
end

figure;
subplot(2,2,[1,3])
bar(categorical({'transp','eins'}),[ t eins]);
hline(t);
title (['Trigger for ' num2str(t) ' trials']);


% check for loss of 1's
dif_time = diff(EEG.times);
dif_latency = diff([EEG.event.latency]);

subplot(2,2,[2,4])
plot([EEG.event.latency]);


%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%                           Reaction times
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% extract trial relevant markers // 1 = Start, 0 = Stimulus start, 2 = trial end

t = 1;
e = 1;
while e < length(EEG.event)
  if contains(EEG.event(e).type,'0-Start')
    EEG.event(e).trial = 1;
    o = e;
    while ~strcmp(EEG.event(o).type,'1') && o < length(EEG.event)-1
      o = o+1;
    end
    EEG.event(o).trial = 0;
    
    te = e;
    while ~strcmp(EEG.event(te).type,'End')
      te = te+1;
    end
    EEG.event(te).trial = 2;
    RT_times(t) = ((EEG.event(te).latency-EEG.event(e).latency)/EEG.srate)*1000; % RT of trial in ms
    
    t = t+1;
  end
  e = e+1;  
  
end

%% plot

ex1 = RT_times(cfg.blck_idx{1,1});
mi1 = RT_times(cfg.blck_idx{2,1});
mi2 = RT_times(cfg.blck_idx{3,1});
ex2 = RT_times(cfg.blck_idx{4,1});

g = [zeros(length(ex1(~isoutlier(ex1))),1);ones(length(mi1),1);2*ones(length(mi2),1);3*ones(length(ex2),1)];

figure;
vs = boxplot([ex1(~isoutlier(ex1)),mi1,mi2,ex2],g)
xticklabels({'EX 1','MI 1','MI 2','EX 2',})
ylabel('[ms]');
xlabel('Blocks');

%% plot raincloud style

% color bar
[cb] = cbrewer('qual','Set3',12,'pchip');

cl(1,:) = cb(4,:);
cl(2,:) = cb(1,:);

%plot handles
e1= raincloud_plot('X', ex1, 'box_on', 1, 'color', cb(1,:),'alpha', 0.5,...
    'box_dodge', 1, 'box_dodge_amount', .15, 'dot_dodge_amount', .35,...
    'box_col_match', 1);
m1= raincloud_plot('X', mi1, 'box_on', 1, 'color', cb(4,:), 'alpha', 0.5,...
    'box_dodge', 1, 'box_dodge_amount', .55, 'dot_dodge_amount', .75,...
    'box_col_match', 1);
m2= raincloud_plot('X', mi2, 'box_on', 1, 'color', cb(2,:), 'alpha', 0.5,...
    'box_dodge', 1, 'box_dodge_amount', .95, 'dot_dodge_amount', 1.15,...
    'box_col_match', 1);
e2= raincloud_plot('X', ex2, 'box_on', 1, 'color', cb(3,:), 'alpha', 0.5,...
    'box_dodge', 1, 'box_dodge_amount', 1.35, 'dot_dodge_amount', 1.55,...
    'box_col_match', 1);
title('Explorative plots pilot RTs')
ylim([-6 6]*0.0001);
set(gca,'XLim', [min(RT_times)*1.1 max(RT_times)*1.1]);


%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%                               ICA
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% inital filtering
EEG = pop_eegfiltnew(EEG, [],HPF_ICA,HPF_ORD_ICA,1,[],1);
EEG = pop_eegfiltnew(EEG, [],LPF_ICA,LPF_ORD_ICA,0,[],1);

%prepare events for ICA
EEG.event=[];
EEG.urevent=[];

t=1;
while t<EEG.pnts+EEG.srate
  EEG.event(end+1).latency=t;
  EEG.event(end).type='t';
  t=t+EEG.srate;
end

EEG = pop_epoch( EEG, { 't' }, [0 1], 'newname', ['pilot_',SUBJ_ID{s},'_temp',], 'epochinfo', 'yes');
N_EPOCHS_BEFORE_ICA(s)=size(EEG.data,3);

EEG = pop_jointprob(EEG,1,[1:EEG.nbchan],PRUNE,PRUNE,0,0);
EEG = pop_rejkurt(EEG,1,[EEG.nbchan],PRUNE,PRUNE,0,0);
EEG = eeg_rejsuperpose(EEG,1,1,1,1,1,1,1,1);
EEG = pop_rejepoch(EEG,EEG.reject.rejglobal,0);
N_EPOCHS_AFTER_ICA(s)=size(EEG.data,3);


EEG.event=[];
EEG.urevent=[];

t=1;
while t<EEG.pnts+EEG.srate
  EEG.event(end+1).latency=t;
  EEG.event(end).type='t';
  t=t+EEG.srate;
end

EEG = pop_epoch( EEG, { 't' }, [0 1], 'newname', ['pilot_',SUBJ_ID{s},'_temp',], 'epochinfo', 'yes');
N_EPOCHS_BEFORE_ICA(s)=size(EEG.data,3);

EEG = pop_jointprob(EEG,1,[1:EEG.nbchan],PRUNE,PRUNE,0,0);
EEG = pop_rejkurt(EEG,1,[EEG.nbchan],PRUNE,PRUNE,0,0);
EEG = eeg_rejsuperpose(EEG,1,1,1,1,1,1,1,1);
EEG = pop_rejepoch(EEG,EEG.reject.rejglobal,0);
N_EPOCHS_AFTER_ICA(s)=size(EEG.data,3);

%% run ICA
EEG = pop_runica(EEG,'icatype','runica','extended',1,'pca',size(EEG.data,1));
EEG = pop_saveset( EEG, 'filename',['pilot_',SUBJ_ID{s},'_ICA.set'],'filepath',PATHOUT2);

