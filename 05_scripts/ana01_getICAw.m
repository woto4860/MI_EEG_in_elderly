%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%                       Get ICA weights
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% PP analysis for frequencyspace patterns in MI
% Data: mad_table (Mareike Daeglau, University of Oldenburg)
% Author: Julius Welzel

% define paths
PATHIN = [path_data 'raw_data\'];
PATHOUT_ICA = [MAIN '04_Data\ana01_ICAw\'];
PATHOUT_SAN = [MAIN '04_Data\ana00_sanity\indv_plots\'];

if ~exist(PATHOUT_ICA)
  mkdir(PATHOUT_ICA);
end

if ~exist(PATHOUT_SAN)
  mkdir(PATHOUT_SAN);
end

% document potential problems with a subject
docError = {};    

%call eeglab
[ALLEEG, EEG, CURRENTSET, ALLCOM] = eeglab;

%% DEFINE CONFIG VARIABLES

% define blocks MI
cfg.tr = 8;
cfg.mi = 40;
cfg.num_block = 4;
cfg.blck_idx = {1:8};
for nb = 2:cfg.num_block
if nb ~=4
  cfg.blck_idx{nb,:} = cfg.blck_idx{end}(end)+1:cfg.blck_idx{end}(end)+cfg.mi;
elseif nb==4
  cfg.blck_idx{nb,:} = cfg.blck_idx{end}(end)+1:cfg.blck_idx{end}(end)+cfg.tr;
end
end

% define blocks LLT
cfg.LLT.num_block = 3;
cfg.LLT.train = 10;
cfg.LLT.test = 32;
cfg.LLT.blck_idx = {1:10};
for nb = 2:cfg.num_block
    cfg.LLT.blck_idx{nb,:} = cfg.LLT.blck_idx{end}(end)+1:cfg.LLT.blck_idx{end}(end)+cfg.LLT.test;
end
  
cfg.n_chan = [1:64];
cfg.channels = {'E01' 'E02' 'E03' 'E04' 'E05' 'E06' 'E07' 'E08' 'E09' 'E10' 'E11' 'E12' 'E13' 'E14' 'E15' 'E16' 'E17' 'E18' 'E19' 'E20' 'E21' 'E22' 'E23' 'E24' 'E25' 'E26' 'E27' 'E28' 'E29' 'E30' 'E31' 'E32' 'E33' 'E34' 'E35' 'E36' 'E37' 'E38' 'E39' 'E40' 'E41' 'E42' 'E43' 'E44' 'E45' 'E46' 'E47' 'E48' 'E49' 'E50' 'E51' 'E52' 'E53' 'E54' 'E55' 'E56' 'E57' 'E58' 'E59' 'E60' 'E61' 'E62' 'E63' 'E64'};

% resampling
cfg.resample = 250;

% Creating variables for ICA
cfg.ICA.LP= 40;
cfg.ICA.HP= 1;
cfg.ICA.prune = 3;

save ([PATHOUT_ICA 'cfg.mat'],'cfg');


%% FIND DATASETS

% find all Datasets
list = dir(fullfile([PATHIN '*MI.xdf']));
SUBJ = extractBefore({list.name},'.');


%% LOAD DATASET

for s = 1:length(SUBJ)
    try
        %% chech if dataset already exists
        if exist([SUBJ{s}(1:2),'_mergeICAw.set'],'file') == 2
            disp(['ICA for ' SUBJ{s} ' has already been cleaned. Continue with next dataset.']);
            pause(0.2);
        else 
            
        % load EEG
        if s == 5
            %data set 40 had to be merged beforehand so it is already a set file
            EEG = pop_loadset('filename',[SUBJ{s} '_corrected.set'],'filepath',PATHIN);
        else
            %load xdf files
            EEG = pop_loadxdf([PATHIN SUBJ{s} '.xdf'], 'streamtype', 'EEG', 'exclude_markerstreams', {});
        end
        
        EEG = pop_editset(EEG, 'setname',['MI_' SUBJ{s}(1:2)]);
        EEG = eeg_checkset(EEG, 'eventconsistency' );

        % clean markers (Mareike Daeglau)
        EEG = renameEvents_table(EEG);
        EEG.event = rmfield(EEG.event, 'del');

        %use 64 channels (omit EMG and others)
        EEG = pop_select( EEG,'channel',cfg.channels);
        %add chanlocs
        EEG = pop_chanedit(EEG, 'lookup',[MAIN '101_software\elec_64ch.elp']);
        % resample
        EEG = pop_resample( EEG,cfg.resample);

        %% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %
        %                       Check Dataset for consistency
        %
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        % OVERVIEW
        % Start marker & counter
        star = 0;
        t = 0;
        eins = 0;
        r = 0;
        op = 0;
        ende = 0;
        c = 1;

        for e = 1:length(EEG.event)

             %count Eventmarker types
             if   ~isempty(strfind(EEG.event(e).type, '-Start-'))
                mark(c).type = EEG.event(e).type;
                mark(c).time = EEG.event(e).latency;
                star = star+1;

             elseif ~isempty(strfind(EEG.event(e).type, 'transp'))
                mark(c).type = EEG.event(e).type;
                mark(c).time = EEG.event(e).latency;
                t = t+1;

             elseif   strcmp(EEG.event(e).type, '1')
                mark(c).type = EEG.event(e).type;
                mark(c).time = EEG.event(e).latency;
                eins = eins+1;

             elseif   ~isempty(strfind(EEG.event(e).type, 'Release'))
                mark(c).type = EEG.event(e).type;
                mark(c).time = EEG.event(e).latency;
                r = r+1;

             elseif   ~isempty(strfind(EEG.event(e).type, 'opaque'))
                mark(c).type = EEG.event(e).type;
                mark(c).time = EEG.event(e).latency;
                op = op+1;

             elseif   ~isempty(strfind(EEG.event(e).type, 'End'))
                mark(c).type = EEG.event(e).type;
                mark(c).time = EEG.event(e).latency;
                ende = ende+1;
             end

             c = c+1;
        end

        figure;
        subplot(2,2,[1,3])
        bar(categorical({'transp','eins'}),[ t eins]);
        hline(t);
        title (['Trigger for ' num2str(t) ' trials']);

        % check for loss of 1's
        dif_time = diff(EEG.times);
        dif_latency = diff([EEG.event.latency]);

        subplot(2,2,[2,4])
        plot([EEG.event.latency]);
        title (['Timecourse of samples']);

        %set figure screensize and save
        set(gcf, 'Position', [0 0 500 300]);
        saveas(gcf,[PATHOUT_SAN SUBJ{s} '_trigger.png']);
        close all;

        %% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %
        %                           Reaction times
        %
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        % extract trial relevant markers // 1 = Start, 0 = Stimulus start, 2 = trial end

        t = 1;
        e = 1;
        while e < length(EEG.event)
          if contains(EEG.event(e).type,'0-Start')
            EEG.event(e).trial = 1;
            o = e;
            while ~strcmp(EEG.event(o).type,'1') && o < length(EEG.event)-1
              o = o+1;
            end
            EEG.event(o).trial = 0;

            te = e;
            while ~strcmp(EEG.event(te).type,'End')
              te = te+1;
            end
            EEG.event(te).trial = 2;
            RT_times(t) = (EEG.event(te).latency-EEG.event(e).latency)*(1000/EEG.srate); % RT of trial in ms

            t = t+1;
          end
          e = e+1;  

        end

        %% plot

        ex1 = RT_times(cfg.blck_idx{1,1});
        mi1 = RT_times(cfg.blck_idx{2,1});
        mi2 = RT_times(cfg.blck_idx{3,1});
        ex2 = RT_times(cfg.blck_idx{4,1});

        g = [zeros(length(ex1(~isoutlier(ex1,'mean'))),1);ones(length(mi1),1);2*ones(length(mi2),1);3*ones(length(ex2(~isoutlier(ex2,'mean'))),1)];

        figure;
        vs = boxplot([ex1(~isoutlier(ex1,'mean')),mi1,mi2,ex2(~isoutlier(ex2,'mean'))],g);
        xticklabels({'EX 1','MI 1','MI 2','EX 2',})
        ylabel('[ms]');
        xlabel('Blocks');

        %set figure screensize and save
        set(gcf, 'Position', [0 0 1000 700]);
        saveas(gcf,[PATHOUT_SAN SUBJ{s} '_RTs.png']);
        close all;

        %% plot raincloud style

        % see pilot script

        %% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %
        %                               ICA
        %
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        % PREPARE MI SET FOR ICA

        % cut between blocks data
        trig = {EEG.event.type}; % all trigger
        trans_trig = ismember(trig,{'transp'}); %trans trigger
        lat_trig_t = [EEG.event(trans_trig).latency]; %latency of trans
        opa_trig = ismember(trig,{'opaque'}); %opaque trigger
        lat_trig_o = [EEG.event(trans_trig).latency]; %latency opaque 
        buff_sec= 4*EEG.srate; % buffer of 5 seconds

        % find starting end end block triggers
        EEG.EndRejTrig(1,:) = [1 lat_trig_t(cfg.blck_idx{1,1}(1))-buff_sec];
        EEG.EndRejTrig(2,:) = [lat_trig_o(cfg.blck_idx{1,1}(end))+buff_sec lat_trig_t(cfg.blck_idx{2,1}(1))-buff_sec];
        EEG.EndRejTrig(3,:) = [lat_trig_o(cfg.blck_idx{2,1}(end))+buff_sec lat_trig_t(cfg.blck_idx{3,1}(1))-buff_sec];
        EEG.EndRejTrig(4,:) = [lat_trig_o(cfg.blck_idx{3,1}(end))+buff_sec lat_trig_t(cfg.blck_idx{4,1}(1))-buff_sec];
        EEG.EndRejTrig(5,:) = [lat_trig_o(cfg.blck_idx{4,1}(end))+buff_sec length(EEG.data)];


        % Reject the Interblock Data
        % save original dataset in new Dataset nEEG
        oEEG = EEG
        % reject before definded parts of the data
        EEG = eeg_eegrej(EEG,EEG.EndRejTrig)

        % plot the original mean of all EEG channels & the deleted windows
        % Content of plot 
        plot_con = mean(oEEG.data(:,:),1);
        % transform position of marker to sec for plotting
        EEG.EndRejTrig = EEG.EndRejTrig./EEG.srate ;


        figure 
        % create timevector
        timevec = (1/EEG.srate:(1/EEG.srate):size(oEEG.data,2)/EEG.srate);
        plot(timevec,plot_con)
        hold on
        % plot rectangles around the deleted blocks
        for q = 1:length(EEG.EndRejTrig);
        rectangle ('Position',[EEG.EndRejTrig(q,1) min(plot_con) [EEG.EndRejTrig(q,2)-EEG.EndRejTrig(q,1)] [max(plot_con)-min(plot_con)]],'FaceColor', [1 0 0 0.5])
        hold on
        end
        xlabel ('[sec]')
        ylabel ('\muV')
        %Set titel which includes the time differences
        amountMI = round(num2str((100-size(EEG.data)/size(oEEG.data)*100)));
        title([num2str(round((length(oEEG.data)-length(EEG.data))/EEG.srate)) ' sec of data were deleted in // ' '( -' amountMI '%) ']) 

        % Save figure
        set(gcf, 'Position', [0 0 1000 700]);
        saveas(gcf,[PATHOUT_SAN, SUBJ{s} ,'_interblock_del.png']);  % raster image
        close

        [ALLEEG EEG CURRENTSET] = eeg_store(ALLEEG, EEG);


        %% PREPARE LLT SET FOR ICA

        % cut between blocks data
        EEG = pop_loadxdf([PATHIN SUBJ{s}(1:2) '_LLT.xdf'], 'streamtype', 'EEG', 'exclude_markerstreams', {});
        EEG= pop_editset(EEG, 'setname',['LLT_' SUBJ{s}]);
        EEG= eeg_checkset(EEG, 'eventconsistency' );

        %use 64 channels (omit EMG and others)
        EEG= pop_select( EEG,'channel',cfg.channels);
        %add chanlocs
        EEG=pop_chanedit(EEG, 'lookup',[MAIN '101_software\elec_64ch.elp']);
        % resample
        EEG= pop_resample( EEG,cfg.resample);

        % find LLT trigger
        all_trig = {EEG.event(:).type};
        an = {'1', '2', '3', '4','empty'};
        not_pic = ismember(all_trig,an);
        pic_i = find(~ismember(all_trig,an));
        pic_trig = all_trig(~not_pic);
        lat_trig_LLT = [EEG.event(pic_i).latency]; %latency of trans
        buff_sec_LLT= 2*EEG.srate; % buffer of 5 seconds

        % find starting end end block triggers
        EEG.EndRejTrig(1,:) = [1 lat_trig_LLT(cfg.LLT.blck_idx{1,1}(1))-buff_sec];
        EEG.EndRejTrig(2,:) = [lat_trig_LLT(cfg.LLT.blck_idx{1,1}(end))+buff_sec lat_trig_LLT(cfg.LLT.blck_idx{2,1}(1))-buff_sec];
        EEG.EndRejTrig(3,:) = [lat_trig_LLT(cfg.LLT.blck_idx{2,1}(end))+buff_sec lat_trig_LLT(cfg.LLT.blck_idx{3,1}(1))-buff_sec];
        EEG.EndRejTrig(4,:) = [lat_trig_LLT(cfg.LLT.blck_idx{3,1}(end))+buff_sec lat_trig_LLT(cfg.LLT.blck_idx{4,1}(1))-buff_sec];
        EEG.EndRejTrig(5,:) = [lat_trig_LLT(cfg.LLT.blck_idx{4,1}(end))+buff_sec length(EEG.data)];

        % Reject the Interblock Data
        % save original dataset in new Dataset nEEG
        oEEG = EEG
        % reject before definded parts of the data
        EEG = eeg_eegrej(EEG,EEG.EndRejTrig)

        % plot the original mean of all EEG channels & the deleted windows
        % Content of plot 
        plot_con = mean(oEEG.data(:,:),1);
        % transform position of marker to sec for plotting
        EEG.EndRejTrig = EEG.EndRejTrig./EEG.srate ;

        figure 
        % create timevector
        timevec = (1/EEG.srate:(1/EEG.srate):size(oEEG.data,2)/EEG.srate);
        plot(timevec,plot_con)
        hold on
        % plot rectangles around the deleted blocks
        for q = 1:length(EEG.EndRejTrig);
        rectangle ('Position',[EEG.EndRejTrig(q,1) min(plot_con) [EEG.EndRejTrig(q,2)-EEG.EndRejTrig(q,1)] [max(plot_con)-min(plot_con)]],'FaceColor', [1 0 0 0.5])
        hold on
        end
        xlabel ('[sec]')
        ylabel ('\muV')
        %Set titel which includes the time differences
        amountLLT = round(num2str((100-size(EEG.data)/size(oEEG.data)*100)));
        title([num2str(round((length(oEEG.data)-length(EEG.data))/EEG.srate)) ' sec of data were deleted in // ' '( -' amountLLT '%) ']) 

        % Save figure
        set(gcf, 'Position', [0 0 1000 700]);
        saveas(gcf,[PATHOUT_SAN, SUBJ{s} ,'_interblock_del_LLT.png']);  % raster image
        close

        [ALLEEG EEG CURRENTSET] = eeg_store(ALLEEG, EEG);

        %% Merge MI & LLT for ICA 
        EEG = pop_mergeset( ALLEEG, [length(ALLEEG)-1 length(ALLEEG)], 0);

        %% Get ICA weights  

        % inital filtering
        EEG = pop_eegfiltnew(EEG, [],cfg.ICA.HP,[],1,[],0);
        EEG = pop_eegfiltnew(EEG, [],cfg.ICA.LP,[],0,[],0);

        %prepare events for ICA
        EEG.event=[];
        EEG.urevent=[];

        t=1;
        while t<EEG.pnts+EEG.srate
          EEG.event(end+1).latency=t;
          EEG.event(end).type='t';
          t=t+EEG.srate;
        end

        EEG = pop_epoch( EEG, { 't' }, [0 1], 'newname', [SUBJ{s},'_merged_epoched',], 'epochinfo', 'yes');
        N_EPOCHS_BEFORE=size(EEG.data,3);

        % pruning
        EEG = pop_jointprob(EEG,1,1:EEG.nbchan,cfg.ICA.prune,cfg.ICA.prune,0,0);
        %EEG = pop_rejkurt(EEG,1,1:EEG.nbchan cfg.ICA.prune,cfg.ICA.prune,0,0);
        EEG = eeg_rejsuperpose( EEG, 1, 1, 1, 1, 1, 1, 1, 1);
        EEG = pop_rejepoch(EEG, EEG.reject.rejglobal ,0);
        N_EPOCHS_AFTER=size(EEG.data,3);

        %% run ICA
        EEG = pop_runica(EEG,'icatype','runica','extended',1,'pca',size(EEG.data,1));

        %% EyeCatch Algorithm to identify eye related artifacts
        eyeDetector = eyeCatch; % create an object from the class
        [isEye, ~, scalpmapObj] = eyeDetector.detectFromEEG(EEG); % detect eye ICs
        eye_numb = find(isEye);   % display the IC numbers for eye ICs (since isEye is a logical array)
        scalpmapObj.plot(isEye);  % plot eye ICs
        saveas(gcf,[PATHOUT_SAN, SUBJ{s}(1:2),'_ICA_eye.png']);  % raster image
        close;
        
        % safe all ICA comps
        pop_topoplot(EEG,0, [1:36] ,'81_merged_epoched',[7 7] ,0,'electrodes','on');
        saveas(gcf,[PATHOUT_SAN, SUBJ{s}(1:2),'_ICA_allcomps.png']);  % raster image
        close;


        ICA.N_EPOCHS_BEFORE = N_EPOCHS_BEFORE;
        ICA.N_EPOCHS_AFTER = N_EPOCHS_AFTER;
        ICA.eyecatching = eye_numb;
        EEG.ICA = ICA;
        EEG.mark = mark;
        EEG.RTs = RT_times;
        EEG.rej.MI= amountMI;
        EEG.rej.LLT = amountLLT;
        
        EEG = pop_saveset( EEG, 'filename',[SUBJ{s}(1:2),'_mergeICAw.set'],'filepath',PATHOUT_ICA);


        end %end for existing dataset
        
        
        
    catch % write in Error doc
        disp(['Error with ' SUBJ{s}])
        docError(end+1) = SUBJ(s);
        close
        continue;
    end
end

save([PATHOUT_ICA 'docError'], 'docError');


