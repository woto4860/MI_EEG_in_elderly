%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%              GMD for juw_el_table // All participants
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% GMD plotting
% Author: Julius Welzel & Mareike Daeglau // University of Oldenburg, 2019
% Version: 1.0 // 29.01.2019 // Initial setup

PATHIN_ERD = [MAIN '04_Data\ana04_erd\'];
PATH_GMD = [MAIN '04_Data\ana05_gmd\'];
PATH_GMD_PLOT = [PATH_GMD 'plots\'];

if ~exist(PATH_GMD_PLOT)
  mkdir(PATH_GMD_PLOT);
end

%load EMG data
load ([PATH_GMD 'GMD.mat']);

cfg.GMD.EEG_ELECS = {'E01' 'E02' 'E03' 'E04' 'E05' 'E06' 'E07' 'E08' 'E09' 'E10' 'E11' 'E12' 'E13' 'E14' 'E15' 'E16' 'E17' 'E18' 'E19' 'E20' 'E21' 'E22' 'E23' 'E24' 'E25' 'E26' 'E27' 'E28' 'E29' 'E30' 'E31' 'E32' 'E33' 'E34' 'E35' 'E36' 'E37' 'E38' 'E39' 'E40' 'E41' 'E42' 'E43' 'E44' 'E45' 'E46' 'E47' 'E48' 'E49' 'E50' 'E51' 'E52' 'E53' 'E54' 'E55' 'E56' 'E57' 'E58' 'E59' 'E60' 'E61' 'E62' 'E63' 'E64'};
cfg.GMD.BL_ms_ep = [-2500:(1000/cfg.resample):0];
cfg.GMD.BL_rERD_ms = [-2000:(1000/cfg.resample):-500];
BL_rERD_ms = cfg.GMD.BL_rERD_ms;
BL_sam = cfg.GMD.BL_ms_ep/(1000/cfg.resample);


%% Calculate GMD for each subject BL vs. MI

b_t = {'PRE','MI 1','MI 2','POST'};

ME_trials = [cfg.blck_idx{[1 4],1}];
MI_trials = [cfg.blck_idx{[2 3],1}];


c_o = 1;
c_y = 1;
for sub = 1:size(ERD,2)
    if str2num(ERD(sub).ID{:})<50;
        map_bl_o_ME(c_o,:) = nanmean(m_rERD_bl(sub,:,ME_trials),3);
        map_bl_o_MI(c_o,:) = nanmean(m_rERD_bl(sub,:,MI_trials),3);
        map_t_o_ME(c_o,:) = nanmean(m_rERD_trial(sub,:,ME_trials),3);
        map_t_o_MI(c_o,:) = nanmean(m_rERD_trial(sub,:,MI_trials),3);
        c_o = c_o+1;
    else
        map_bl_y_ME(c_y,:) = nanmean(m_rERD_bl(sub,:,ME_trials),3);
        map_bl_y_MI(c_y,:) = nanmean(m_rERD_bl(sub,:,MI_trials),3);
        map_t_y_ME(c_y,:) = nanmean(m_rERD_trial(sub,:,ME_trials),3);
        map_t_y_MI(c_y,:) = nanmean(m_rERD_trial(sub,:,MI_trials),3);
        c_y = c_y+1;

    end
end


%% Get GMD

% old
[mGMD_o(1) semGMD_o(1) GMD_o_ME] = GMD_group(map_bl_o_ME,map_t_o_ME);
[mGMD_o(2) semGMD_o(2) GMD_o_MI] = GMD_group(map_bl_o_MI,map_t_o_MI);

% young
[mGMD_y(1) semGMD_y(1) GMD_y_ME] = GMD_group(map_bl_y_ME,map_t_y_ME);
[mGMD_y(2) semGMD_y(2) GMD_y_MI] = GMD_group(map_bl_y_MI,map_t_y_MI);


g=figure;
subplot(2,6,[1,2,7,8])
errorbar(mGMD_o,semGMD_o,'-or','color','r')
hold on
errorbar(mGMD_y,semGMD_y,'-og','color','g')
       
xlim([0 3])
ylabel 'GMD'
xticks([1:2])
xticklabels({'ME','MI'})
legend ('Old','Young')
title 'GMD group differences between BL & TRIAL'
set(gca, 'box', 'off')       

% topoplots
%old
subplot(2,6,3)
topoplot(zscore(nanmean(map_bl_o_ME)),cfg.chanlocs,'numcontour',0)
title 'BL OLD ME'
colorbar


subplot(2,6,4)
topoplot(zscore(nanmean(map_t_o_ME)),cfg.chanlocs,'numcontour',0)
title 'TRIAL OLD ME'
colorbar


subplot(2,6,5)
topoplot(zscore(nanmean(map_bl_o_MI)),cfg.chanlocs,'numcontour',0)
title 'BL OLD MI'
colorbar


subplot(2,6,6)
topoplot(zscore(nanmean(map_t_o_MI)),cfg.chanlocs,'numcontour',0)
title 'TRIAL OLD MI'
colorbar


%young
subplot(2,6,9)
topoplot(zscore(nanmean(map_bl_y_ME)),cfg.chanlocs,'numcontour',0)
title 'BL YOUNG ME'
colorbar


subplot(2,6,10)
topoplot(zscore(nanmean(map_t_y_ME)),cfg.chanlocs,'numcontour',0)
title 'TRIAL YOUNG ME'
colorbar


subplot(2,6,11)
topoplot(zscore(nanmean(map_bl_y_MI)),cfg.chanlocs,'numcontour',0)
title 'BL YOUNG MI'
colorbar


subplot(2,6,12)
topoplot(zscore(nanmean(map_t_y_MI)),cfg.chanlocs,'numcontour',0)
colorbar
title 'TRIAL YOUNG MI'

%%

save_fig(gcf,PATH_GMD_PLOT,'GMD_overview')
