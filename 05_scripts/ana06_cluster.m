%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%   cluster topographical differences for juw_el_table // All participants
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Cluster analysis
% Author: Julius Welzel // University of Oldenburg, 2019
% Version: 1.0 // 28.02.2019 // Initial setup

% define paths
PATHIN_ERD =  [MAIN '04_Data\ana04_erd\'];
PATHIN_RT = [MAIN '04_Data\'];
PATHIN_EMG = [MAIN '04_Data\ana03_emg\'];


PATHOUT_CLUSTER = [MAIN '04_Data\ana06_cluster\'];

if ~exist(PATHOUT_CLUSTER)
  mkdir(PATHOUT_CLUSTER);
end 

list = dir(fullfile([PATHIN_RT 'ana02_clean\*clean*.set']));
SUBJ = extractBefore({list.name},'_');

load([PATHIN_ERD 'ERD_mue.mat']);
load([PATHIN_ERD 'cfg_mue.mat']);
load([PATHIN_EMG 'emg_all.mat']);

% define trails of interest
ME_trials = [cfg.blck_idx{[1 4],1}];
MI_trials = [cfg.blck_idx{[2 3],1}];

SUBJ = str2double(SUBJ);
old = (SUBJ>=80);
young = (SUBJ<80);

%% Define channel distances :)
dis_chan = [cfg.chanlocs.X;cfg.chanlocs.Y;cfg.chanlocs.Z];
dis_mat_chan = distmat(dis_chan');

% get cartesian coordinates by MIKE X COHEN :D and grid data
[elocsX,elocsY] = pol2cart(pi/180*[cfg.chanlocs.theta],[cfg.chanlocs.radius]);
elocsX = elocsX*-1; %swap left/ right
xlin = linspace(min(elocsX),max(elocsX),200);
ylin = linspace(min(elocsY),max(elocsY),200);
[Xgrid Ygrid] = meshgrid(xlin,ylin);
top_old = zscore(double(nanmean(nanmean(m_rERD_trial(old,:,MI_trials),3))));
top_young = zscore(double(nanmean(nanmean(m_rERD_trial(young,:,MI_trials),3))));

% calculate area for each trial
surf_old = {};
surf_young = {};
for s = find(young)
    disp(['SUBJ: ' num2str(SUBJ(s))])
   for t = MI_trials
       
       topo = zscore((m_rERD_trial(s,:,t)));
       surf_st = griddata(elocsX,elocsY,topo,Xgrid,Ygrid,'cubic');
       top_thresh = min(min(topo))*0.5;
       surf_st(surf_st>top_thresh) = NaN;
       surf_young{end+1} = SurfArea(surf_st,Xgrid,Ygrid);
       
   end
end

for s = find(old)
    disp(['SUBJ: ' num2str(SUBJ(s))])
   for t = MI_trials
       
       topo = zscore((m_rERD_trial(s,:,t)));
       surf_st = griddata(elocsX,elocsY,topo,Xgrid,Ygrid,'cubic');
       top_thresh = min(min(topo))*0.5;
       surf_st(surf_st>top_thresh) = NaN;
       surf_old{end+1} = SurfArea(surf_st,Xgrid,Ygrid);
       
   end
end




%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%                     RTs & ERD lateralisation dist
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% extract trial relevant markers // 1 = Start, 0 = Stimulus start, 2 = trial end
load ([PATHIN_ERD 'RTs.mat']);

t = 1;
e = 1;

%% RTs per block
for t = 1:length(RT)

    ex1(:,t) = RT(t).times(cfg.blck_idx{1,1});
    ex1(~emg_check(t).trials_valid(cfg.blck_idx{1,1}),t) = NaN;
    mi1(:,t) = RT(t).times(cfg.blck_idx{2,1});
    mi2(:,t) = RT(t).times(cfg.blck_idx{3,1});
    if length(RT(t).times)>100
        ex2(:,t) = RT(t).times(249:256);
        ex2(~emg_check(t).trials_valid(249:256),t) = NaN;

    else
        ex2(:,t) = RT(t).times(cfg.blck_idx{4,1});
        ex2(~emg_check(t).trials_valid(cfg.blck_idx{4,1}),t) = NaN;

    end
    
end

%% indices for scatterhist
% size of all inputs
s_idx_vec = [1 prod(size(mi1))+prod(size(mi2))];
g_idx = zeros(40,34);
g_idx(:,old) = 1;
g_idx_vec = reshape([g_idx,g_idx],s_idx_vec);
blck_idx = reshape([ones(40,34),2*ones(40,34)],s_idx_vec);

%% ERD values per block

cfg.chan.all = 1:64;
cfg.chan.motor = cfg.chan.all(~ismember(cfg.chan.all,[19,20,60,51,7,45,33,46,52]));
cfg.chan.ROI_lat{1} = sort([31,5,41,30,40,49]); %contralateral side
cfg.chan.ROI_lat{2} = sort([28,3,29,37,38,48]); %ipsilateral side

%vec with all RTs
RT_vec = reshape([mi1,mi2],s_idx_vec);

%vec with all ERDs
ERD_lat_mi1 = squeeze(nanmean(m_rERD_trial(:,cfg.chan.ROI_lat{1},cfg.blck_idx{2,1}),2))'-squeeze(nanmean(m_rERD_trial(:,cfg.chan.ROI_lat{2},cfg.blck_idx{2,1}),2))';
ERD_lat_mi2= squeeze(nanmean(m_rERD_trial(:,cfg.chan.ROI_lat{1},cfg.blck_idx{3,1}),2))'-squeeze(nanmean(m_rERD_trial(:,cfg.chan.ROI_lat{2},cfg.blck_idx{3,1}),2))';
ERD_vec = reshape([ERD_lat_mi1,ERD_lat_mi2],s_idx_vec);

%%
figure
scatterhist(RT_vec(blck_idx==1),ERD_vec(blck_idx==1),'Group',g_idx_vec(blck_idx==1),'Kernel','on','Color',[c_young;c_old])
legend('Young','Old')
xlabel 'RTs [ms]'
ylabel 'Lateralisation [%]'
title 'Trials RTs vs. Lateralisation for all participants MI1'
save_fig(gcf,PATHOUT_CLUSTER,'RTvsLAT_MI1','fontsize',15,'figsize',[0 0 20 15]);

figure
scatterhist(RT_vec(blck_idx==2),ERD_vec(blck_idx==2),'Group',g_idx_vec(blck_idx==2),'Kernel','on','Color',[c_young;c_old])
legend('Young','Old')
xlabel 'RTs [ms]'
ylabel 'Lateralisation [%]'
title 'Trials RTs vs. Lateralisation for all participants MI2'
save_fig(gcf,PATHOUT_CLUSTER,'RTvsLAT_MI2','fontsize',15,'figsize',[0 0 20 15]);

figure
scatterhist(RT_vec,ERD_vec,'Group',g_idx_vec,'Kernel','on','Color',[c_young;c_old])
legend('Young','Old')
xlabel 'RTs [ms]'
ylabel 'Lateralisation [%]'
title ({'Trials RTs vs. Lateralisation','All participants MI1 & MI2'})
save_fig(gcf,PATHOUT_CLUSTER,'RTvsLAT_MI12','fontsize',25,'figsize',[0 0 35 27]);

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%                    MDS all trials
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clust_old = mds_cent(nanmean(m_rERD_trial(old,:,MI_trials),3));
clust_young = mds_cent(nanmean(m_rERD_trial(young,:,MI_trials),3));

plot(clust_old(:,1),clust_old(:,2),'.','MarkerSize',25,'Color',c_old);
hold on
plot(clust_young(:,1),clust_old(:,2),'.','MarkerSize',25,'Color',c_young);
legend('Old','Young')
title 'MDS clsutering (64 channel)'
save_fig(gcf,PATHOUT_CLUSTER,'MDS_64_chan','fontsize',20,'figsize',[0 0 15 15]);

