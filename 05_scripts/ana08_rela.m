%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%   Topo diff with RTs in juw_el_table // Both groups
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Cortical area with RTs
% Author: Julius Welzel // University of Oldenburg, 2019
% Version: 1.0 // 17.03.2019 // Initial setup


% define paths
PATHIN_TOPDIFF = [MAIN '04_Data\ana07_topdiff\'];
PATHIN_SUBJ = [MAIN '04_Data\'];
PATHIN_RTs = [MAIN '04_Data\ana04_erd\'];
PATHIN_EMG = [MAIN '04_Data\ana03_emg\'];
PATHIN_BE = [MAIN '04_Data\ana11_behavioural\'];


PATHOUT_SCAT = [MAIN '04_Data\ana08_rela\'];

if ~exist(PATHOUT_SCAT)
  mkdir(PATHOUT_SCAT);
end 

%define FBs
FB = {'mue','beta','broad'};

%load data
load([PATHIN_RTs 'cfg.mat']);
load ([PATHIN_EMG 'emg_all.mat']);
load ([PATHIN_EMG 'EMG_ES.mat']);
load ([PATHIN_RTs 'RTs.mat']);
load([PATHIN_BE 'behavioural.mat']);
cfg.n_trials = 96;


for fb = 1:length(FB)
    area(fb).name = FB{fb};
    data = load([PATHIN_TOPDIFF 'inter_area_' FB{fb} '.mat']);
    area(fb).data = data.area_z_t_avg;
end


list = dir(fullfile([PATHIN_SUBJ 'ana02_clean\*clean*.set']));
SUBJ = extractBefore({list.name},'_');
SUBJ = str2double(SUBJ);
old = (SUBJ>=80);
young = (SUBJ<80);

% define trails of interest
ME_trials = [cfg.blck_idx{[1 4],1}];
MI_trials = [cfg.blck_idx{[2 3],1}];

%define FBs
FB = {'mue','beta','broad'};


%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%               Get delta ME-MI in ms from RTs
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

old = (SUBJ>=80);
young = (SUBJ<80);
g_idx = ones(1,(length(RT)));
g_idx(old) = 2;

blocks = cell(1,cfg.n_trials);
blocks(cfg.blck_idx{1,1}) = {'EX1'};
blocks(cfg.blck_idx{2,1}) = {'MI1'};
blocks(cfg.blck_idx{3,1}) = {'MI2'};
blocks(cfg.blck_idx{4,1}) = {'EX2'};


for t = 1:length(RT)

    ex1(:,t) = RT(t).times(cfg.blck_idx{1,1});
    ex1(~emg_check(t).trials_valid(cfg.blck_idx{1,1}),t) = NaN;
    mi1(:,t) = RT(t).times(cfg.blck_idx{2,1});
    mi2(:,t) = RT(t).times(cfg.blck_idx{3,1});
    
    if length(RT(t).times)>100
        ex2(:,t) = RT(t).times(249:256);
        ex2(~emg_check(t).trials_valid(249:256),t) = NaN;

    else
        ex2(:,t) = RT(t).times(cfg.blck_idx{4,1});
        ex2(~emg_check(t).trials_valid(cfg.blck_idx{4,1}),t) = NaN;

    end
    
end

RT_all = [ex1;mi1 ;mi2 ;ex2];

diff_RT = nanmean(RT_all(ME_trials,:))-nanmean(RT_all(MI_trials,:));
diff_RT_o = nanmean([ex1(:,old);ex2(:,old)])-nanmean([mi1(:,old);mi2(:,old)]);
diff_RT_y = nanmean([ex1(:,young);ex2(:,young)])-nanmean([mi1(:,young);mi2(:,young)]);

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%                     relate Age to intersect area
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% find maximal diff in intersect area between young & old

age_all = table2array(behavioural(:,3));
sub_age = table2array(behavioural(:,1));
[never idx] = ismember(SUBJ,sub_age)
age_all = age_all(idx);

for fb = 1:length(FB)
    
    figure
    % find maximal diff in intersect area between young & old
    [v i] = max(mean(area(fb).data(old,:))-mean(area(fb).data(young,:)));
    [m_a_old, se_a_old] = mean_SEM(area(fb).data(old,:)');
    [m_a_young, se_a_young] = mean_SEM(area(fb).data(young,:)');
    i = ([m_a_old-se_a_old]-[m_a_young+se_a_young])>0;

    scatter(age_all,mean(area(fb).data(:,i),2))
    l = lsline;
    hold on
    l(1).LineWidth = 1.5;
    l(1).Color = 'k';
    sc = gscatter(age_all,mean(area(fb).data(:,i),2),g_idx,[],[],25)
    corr_age = round(corr(age_all,mean(area(fb).data(:,i),2)),2);
    sc(1).Color = c_young;
    sc(2).Color = c_old;


%     title ([area(fb).name])
    legend ([ l(1) sc(2) sc(1)],{['r: ' num2str(corr_age)],'Old','Young'},'Location','south')
    legend boxoff
    xlabel 'Age [years]'
    ylabel 'Area at intersect [u^2]'
    box off
    
    pic_r = 1/1.7;
    save_fig(gcf,PATHOUT_SCAT,['area_age_' FB{fb}],'fontsize',20,'figsize',[0 0 22.7 pic_r*22.7]);

end

    
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%                     delta RT to intersect area
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% find maximal diff in intersect area between young & ol  
n_intersect = 24;

for fb = 1:length(FB)
    
    figure
    % find maximal diff in intersect area between young & old
    [v i] = max(mean(area(fb).data(old,:))-mean(area(fb).data(young,:)));
    [m_a_old, se_a_old] = mean_SEM(area(fb).data(old,:)');
    [m_a_young, se_a_young] = mean_SEM(area(fb).data(young,:)');
    full_w_y = fwhm(1:n_intersect,m_a_young);
    full_w_o = fwhm(1:n_intersect,m_a_old);
    i_y = [round(find(m_a_young==max(m_a_young))-0.5*full_w_y):round(find(m_a_young==max(m_a_young))+0.5*full_w_y)];
    i_o = [round(find(m_a_old==max(m_a_old))-0.5*full_w_o):round(find(m_a_old==max(m_a_old))+0.5*full_w_o)];



%     sc = gscatter(diff_RT,mean(area(fb).data(:,i),2),g_idx,[],[],25)
    sc_o = scatter(diff_RT(:,old),sum(area(fb).data(old,i_o),2),'filled','MarkerFaceColor',c_old);hold on;
    sc_y = scatter(diff_RT(:,young),sum(area(fb).data(young,i_y),2),'filled','MarkerFaceColor',c_young);hold on;

    corr_y = round(corr(diff_RT(:,young)',sum(area(fb).data(young,i_y),2)),2);
    corr_o = round(corr(diff_RT(:,old)',sum(area(fb).data(old,i_o),2)),2);
%     sc(1).Color = c_young;
%     sc(2).Color = c_old;
    l = lsline;
    l(1).LineWidth = 1.5;
    l(1).Color = c_young;
    l(2).LineWidth = 1.5;
    l(2).Color = c_old;
%     title ([area(fb).name ' Mean'])
    legend ([sc_o sc_y],{['Old / r: ' num2str(corr_o)],['Young / r: ' num2str(corr_y)]},'Location','best');
    legend boxoff
    xlabel '\Delta ME-MI [ms]'
    ylabel 'Area at intersect [u^2]'
    box off
    
    pic_r = 1/1.3;
    save_fig(gcf,PATHOUT_SCAT,['area_dRT_' FB{fb}],'fontsize',20,'figsize',[0 0 22.7 pic_r*22.70]);
    
    % Stats for area
    stat_area(fb).ttest = ttest2_param(sum(area(fb).data(young,i_y),2),sum(area(fb).data(old,i_o),2));
    stat_area(fb).ttest.name = FB{fb};
    
    % Stats on distr params
    load ([PATHIN_TOPDIFF 'dist_param_' FB{fb} '.mat'])
    stat_kurt(fb).ttest = ttest2_param(kurt_o,kurt_y);
    stat_kurt(fb).ttest.name = FB{fb};
    stat_skew(fb).ttest = ttest2_param(skew_o,skew_y);
    stat_skew(fb).ttest.name = FB{fb};


end




