%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%              GMD for juw_el_table // All participants
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% GMD Main
% Author: Julius Welzel // University of Oldenburg, 2019
% Version: 1.0 // 18.12.2019 // Initial setup

PATHIN_ICA = [MAIN '04_Data\ana02_clean\'];
PATHIN_EMG = [MAIN '04_Data\ana03_emg\'];
PATHIN_ERD = [MAIN '04_Data\ana04_erd\'];
PATHOUT_GMD = [MAIN '04_Data\ana05_gmd\'];

if ~exist(PATHOUT_GMD)
  mkdir(PATHOUT_GMD);
end

list = dir(fullfile([PATHIN_ICA '*clean*.set']));
SUBJ = extractBefore({list.name},'_');

%load EMG data
load ([PATHIN_EMG 'EMG_ES.mat']); %includes cfg file
load ([PATHIN_EMG 'emg_all.mat']); 


%% Define params for ERD ME analyses to select ROI
cfg.GMD.HP = [8 15];
cfg.GMD.LP = [12 30];
cfg.GMD.EEG_ELECS = {'E01' 'E02' 'E03' 'E04' 'E05' 'E06' 'E07' 'E08' 'E09' 'E10' 'E11' 'E12' 'E13' 'E14' 'E15' 'E16' 'E17' 'E18' 'E19' 'E20' 'E21' 'E22' 'E23' 'E24' 'E25' 'E26' 'E27' 'E28' 'E29' 'E30' 'E31' 'E32' 'E33' 'E34' 'E35' 'E36' 'E37' 'E38' 'E39' 'E40' 'E41' 'E42' 'E43' 'E44' 'E45' 'E46' 'E47' 'E48' 'E49' 'E50' 'E51' 'E52' 'E53' 'E54' 'E55' 'E56' 'E57' 'E58' 'E59' 'E60' 'E61' 'E62' 'E63' 'E64'};
cfg.GMD.BL_ms_ep = [-2500:(1000/cfg.resample):0];
cfg.GMD.BL_rERD_ms = [-2500:(1000/cfg.resample):-500];
BL_rERD_ms = cfg.GMD.BL_rERD_ms;
BL_sam = cfg.GMD.BL_ms_ep/(1000/cfg.resample);



%% process data again for different FBs
for sub = 1:size(SUBJ,2)
    
    % load dataset 
    if str2num(SUBJ{sub})<50;    
        EEG = pop_loadset('filename',[SUBJ{sub},'_clean_ICA.set'],'filepath',PATHIN_ICA);
    else
        EEG = pop_loadset('filename',[SUBJ{sub},'_clean.set'],'filepath',PATHIN_ICA);
    end
    
    %checkset to re-order trigger via latency (different triggers are appended in LSL)
    EEG = eeg_checkset(EEG, 'eventconsistency' );
    %use 64 channels (omit EMG and others)
    EEG = pop_select( EEG,'channel',cfg.GMD.EEG_ELECS);
    %add chanlocs
    EEG = pop_chanedit(EEG, 'lookup',[MAIN '101_Software\' 'elec_64ch.elp']);
    %rename LSL-marker
    EEG = renameEvents_table(EEG);
    %rename redundant triggers
    EEG = triggerhand(EEG);
    %low-pass filter 30 Hz
    EEG = pop_firws(EEG, 'fcutoff', cfg.GMD.LP(2), 'ftype', 'lowpass', 'wtype', 'hamming', 'forder', 166);
    %resample to 250 Hz
    EEG = pop_resample(EEG, cfg.resample);
    %high-pass filter 1 Hz
    EEG = pop_firws(EEG, 'fcutoff', cfg.GMD.HP(1), 'ftype', 'highpass', 'wtype', 'hamming', 'forder', 414); 
    
    ERD(sub).ID = SUBJ(sub);
    
    %% Epoch data with power
    
    % take different trials into account
    if str2num(emg_check(sub).ID)<50;    
        trials = [cfg.blck_idx{1}(1):cfg.blck_idx{3}(end) 249:256];
    else
        trials = [cfg.blck_idx{1}(1):cfg.blck_idx{4}(end)];
    end
    
    count=0;
    count_ep = 1;
    for h = 1:size(EEG.event,2) % loop through all events
        
        if strcmp(EEG.event(h).type, '1') % find trial onset
            
                begin_epoch = EEG.event(h).latency;
                flag = true;

        elseif strcmp(EEG.event(h).type, 'End') && flag == true
            
            count = count+1;
            %skip unneccessary epochs
            if count>trials(cfg.blck_idx{3}(end)) && count<trials(cfg.blck_idx{4}(1)) && str2num(SUBJ{sub})<50;continue;end      
            
            last_epoch = EEG.event(h).latency;
            bp_lat = cfg.EMG.thresh_BP(sub); %ES BP time in ms
            ERD(sub).filt_ep{count_ep}(:,:) = EEG.data(:,(begin_epoch+BL_sam(1)):last_epoch-(bp_lat/(1000/cfg.resample))).^2; % EEG data per trial // + BL // - BP time // square for ERD

            flag = false;
            count_ep = count_ep+1;

        end

    end % ind trials
 
%% relative ERD for all trials

    % take different trials into account

    % calculate relative power for single trial BL & MI
    for t = 1:length(ERD(sub).filt_ep)
        %single trial
        time_ep_ms = [cfg.GMD.BL_ms_ep(1):(1000/EEG.srate):(length(ERD(sub).filt_ep{t})+BL_sam(1))*(1000/EEG.srate)-1];
        ERD(sub).time_ep_ms{t} = time_ep_ms;
        BL_rERD_time_log =  ismember(time_ep_ms,BL_rERD_ms);
        BL_rERD_st = mean(ERD(sub).filt_ep{(t)}(:,BL_rERD_time_log),2); %Single trial baseline
        rERD_blc{sub,t} = ((ERD(sub).filt_ep{(t)}-BL_rERD_st)./repmat(BL_rERD_st,1,length(ERD(sub).filt_ep{(t)})))*100;

        % average for ERD all trials
        %rERD baseline
        m_rERD_bl(sub,:,t) = nanmean(rERD_blc{sub,t}(:,BL_rERD_time_log),2); %average over BL timepoints with all values
        
        %rERD trial
        auc_ERD = rERD_blc{sub,t}(:,:)<0; %after onset store logical for every ERD smaller 0
        rERD_blc{sub,t}(~auc_ERD) = NaN; % all ERD
        m_rERD_trial(sub,:,t) = nanmean(rERD_blc{sub,t}(:,time_ep_ms>0),2); %average over trial with time bigger 0 -> after '1'



        if isnan(EMG_all(sub,t)) | emg_check(sub).trials_valid(t) == 0
            m_rERD_trial(sub,:,t) = NaN;
            m_rERD_bl(sub,:,t) = NaN;
        end
    end % trial

end

%%
save([PATHOUT_GMD 'GMD.mat'],'ERD','m_rERD_trial','m_rERD_bl');
save([PATHOUT_GMD 'cfg.mat'],'cfg');