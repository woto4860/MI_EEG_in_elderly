%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%              ERD topoplot for juw_el_table // All participants
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ERD plots
% Author: Julius Welzel // University of Oldenburg, 2019
% Version: 1.0 // 24.12.2019 // Initial setup

PATHIN_ICA = [MAIN '04_Data\ana02_clean\'];
PATHIN_EMG = [MAIN '04_Data\ana03_emg\'];
PATHOUT_ERD = [MAIN '04_Data\ana04_erd\'];

if ~exist(PATHOUT_ERD)
  mkdir(PATHOUT_ERD);
end

list = dir(fullfile([PATHIN_ICA '*clean*.set']));
SUBJ = extractBefore({list.name},'_');
young = [1:16,34];
old = 17:33;

% set colors
color = redblue(100);
FB = {'mue','beta','broad'};

% load rERD file
load([PATHOUT_ERD 'cfg.mat']);
load([PATHIN_EMG 'emg_all.mat']); %emg files

%% loop over FBs

for fb = 1:length(FB)
    
load([PATHOUT_ERD 'ERD_' FB{fb} '.mat']);

%% define trails of interest
ME_trials = [cfg.blck_idx{[1 4],1}];
MI_trials = [cfg.blck_idx{[2 3],1}];

% z-score over all
z_top_st = nanzscore(m_rERD_trial,[],2);

%% Topoplots mean ERD & z-score ERD rERD values below mean BL
mERD_ME = nanmean(nanmean(m_rERD_trial(:,:,ME_trials),3),1);
zERD_ME = zscore(mERD_ME);
mERD_MI = nanmean(nanmean(m_rERD_trial(:,:,MI_trials),3),1);
zERD_MI = zscore(mERD_MI);


figure;
topoplot(mERD_ME,cfg.chanlocs,'colormap',color)
hc=colorbar;
caxis([min(mERD_ME)-3 max(mERD_ME)+3])
xlabel(hc,'ERD [%]');
title (['Group average ERD ' FB{fb} ' for all ME trials']);
save_fig(gcf,[PATHOUT_ERD 'plots\'],['mERD_ME_' FB{fb}]);


figure;
topoplot(zERD_ME,cfg.chanlocs,'colormap',color)
hc=colorbar;
xlabel(hc,'z-value');
title (['Group average z-scored ERD ' FB{fb} ' for all ME trials']);
save_fig(gcf,[PATHOUT_ERD 'plots\'],['zERD_ME_' FB{fb}]);

figure;
topoplot(mERD_MI,cfg.chanlocs,'colormap',color)
hc=colorbar;
caxis([min(mERD_MI)-3 max(mERD_MI)+3])
xlabel(hc,'ERD [%]');
title (['Group average ERD ' FB{fb} ' for all MI trials']);
save_fig(gcf,[PATHOUT_ERD 'plots\'],['mERD_MI_' FB{fb}]);


figure;
topoplot(zERD_MI,cfg.chanlocs,'colormap',color)
hc=colorbar;
xlabel(hc,'z-value');
title (['Group average z-scored ERD ' FB{fb} ' for all MI trials']);
save_fig(gcf,[PATHOUT_ERD 'plots\'],['zERD_MI_' FB{fb}]);

%% Topoplots mean ERD & z-score ERD all rERD values
mERD_ME = nanmean(nanmean(m_rERD_trial_all(:,:,ME_trials),3),1);
zERD_ME = zscore(mERD_ME);
mERD_MI = nanmean(nanmean(m_rERD_trial_all(:,:,MI_trials),3),1);
zERD_MI = zscore(mERD_MI);


figure;
topoplot(mERD_ME,cfg.chanlocs,'colormap',color)
hc=colorbar;
caxis([min(mERD_ME)-3 max(mERD_ME)+3])
xlabel(hc,'ERD [%]');
title (['Group average ERD ' FB{fb} ' for all ME trials']);
save_fig(gcf,[PATHOUT_ERD 'plots\'],['all_mERD_ME_' FB{fb}]);


figure;
topoplot(zERD_ME,cfg.chanlocs,'colormap',color)
hc=colorbar;
xlabel(hc,'z-value');
title (['Group average z-scored ERD ' FB{fb} ' for all ME trials']);
save_fig(gcf,[PATHOUT_ERD 'plots\'],['all_zERD_ME_' FB{fb}]);

figure;
topoplot(mERD_MI,cfg.chanlocs,'colormap',color)
hc=colorbar;
caxis([min(mERD_MI)-3 max(mERD_MI)+3])
xlabel(hc,'ERD [%]');
title (['Group average ERD ' FB{fb} ' for all MI trials']);
save_fig(gcf,[PATHOUT_ERD 'plots\'],['all_mERD_MI_' FB{fb}]);


figure;
topoplot(zERD_MI,cfg.chanlocs,'colormap',color)
hc=colorbar;
xlabel(hc,'z-value');
title (['Group average z-scored ERD ' FB{fb} ' for all MI trials']);
save_fig(gcf,[PATHOUT_ERD 'plots\'],['all_zERD_MI_' FB{fb}]);

%% Topo per MI per group
z_y = max(abs([max(nanmean(nanmean(z_top_st(old,:,MI_trials),3))) min(nanmean(nanmean(z_top_st(old,:,MI_trials),3)))]));
z_o = max(abs([max(nanmean(nanmean(z_top_st(young,:,MI_trials),3))) min(nanmean(nanmean(z_top_st(young,:,MI_trials),3)))]));
z_v = round(max([z_o z_y]),1);
f_s = 15;

figure;
topoplot(nanmean(nanmean(z_top_st(old,:,MI_trials),3)),cfg.chanlocs,'colormap',color,'style','map')
hc=colorbar;
caxis ([-z_v z_v])
xlabel(hc,'z-Score [\sigma]');
save_fig(gcf,[PATHOUT_ERD 'plots\'],['zERD_MI_old_' FB{fb}],'fontsize',30,'figsize',[0 0 f_s+5 f_s]);

figure;
topoplot(nanmean(nanmean(z_top_st(young,:,MI_trials),3)),cfg.chanlocs,'colormap',color,'style','map')
hc=colorbar;
caxis ([-z_v z_v])
xlabel(hc,'z-Score [\sigma]');
save_fig(gcf,[PATHOUT_ERD 'plots\'],['zERD_MI_young_' FB{fb}],'fontsize',30,'figsize',[0 0 f_s+5 f_s]);


%% ERD topo pre//post
blck = [1 4 2 3];
b_t = {'PRE','MI 1','MI 2','POST'};


g_name = {'young','old'};
g_idx = {young,old};

for g = 1:length(g_name)
    figure
    for b = 1:4

%         mERD = nanmean(nanmean(m_rERD_trial(g_idx{g},:,cfg.blck_idx{blck(b)}),3),1);
        zERD = nanmean(nanmean(z_top_st(g_idx{g},:,cfg.blck_idx{blck(b)}),3),1);
        subplot(2,2,b)
        topoplot(zERD,cfg.chanlocs,'style','map','colormap',color)
        hc=colorbar;
        xlabel(hc,'z-Score [\sigma]');
%         caxis ([min(min(zERD)) max(max(zERD))])
        title(b_t{blck(b)},'fontsize',12);

    end

%     sgtitle({'AVERAGE for ' g_name{g} ' // 8-30 Hz // "1":"End"'});

    save_fig(gcf,[PATHOUT_ERD 'plots\'],['ROI_blck_' g_name{g} '_' FB{fb}],'fontsize',20);
end


%% Decide on ROI
%{
cfg.chan.all = 1:64;
cfg.chan.ROI{1} = cfg.chan.all;

precentile_ROI = 100-[87.5 90 92.5 95];

roi_f = figure;
c_s = 1;

for r = 1:length(precentile_ROI)
    for c = 1:length(cfg.chan.ROI)
        roi_perc =  prctile(zERD_ME(cfg.chan.ROI{c}),precentile_ROI(r));
        [chan_zScore chan_rank] = find(zERD_ME(cfg.chan.ROI{c}) < roi_perc);

        subplot(2,2,c_s);
        topoplot(zERD_ME,cfg.chanlocs,'style','map','plotchans',cfg.chan.ROI{c},'emarker',{'.','k',5,1},'emarker2',{chan_rank,'o','w',4,1});
        hc=colorbar;
        xlabel(hc,'z-value');
        title(['Percentile ' num2str(precentile_ROI(r)) ' ROI chan: ' num2str(chan_rank)]);
        c_s = c_s+1;
    end
end

% sgtitle(['Electrodes of lowest percentile for subsets to define ROI from ME']);

save_fig(gcf,[PATHOUT_ERD 'plots\'],'ROI_select_mue','fontsize',12,'figsize',[0 0 35 25]);

%}

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%              ERD topoplot for juw_el_table // Individual plots 
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

 for s = 1:size(m_rERD_trial,1)
   
    blck = [1 2 3 4];
    b_t = {'PRE','MI 1','MI 2','POST'};

    figure
    for b = 1:4

        mERD = nanmean(nanmean(m_rERD_trial(s,:,cfg.blck_idx{blck(b)}),3),1);
        zERD = zscore(mERD);
        subplot(2,4,b)
        topoplot(zERD,cfg.chanlocs,'colormap',color)
        hc=colorbar;
        xlabel(hc,'z-value');
        title(b_t{blck(b)},'fontsize',12);

    end
    
    % variance per epoch
    for t = 1:size(ERD(s).filt_ep,2)
        chan_var(:,t) = var([ERD(s).filt_ep{t}],0,2);
    end
    
    ep_chan_var = mean(chan_var,1);
    art_ep = squeeze(any(isnan(m_rERD_trial(s,:,:))));
    
    subplot(2,4,[5 8])
    a = gscatter(1:96,ep_chan_var,double(art_ep),'br','ox')
    set(a(1), 'MarkerFaceColor', 'b')
    if sum(art_ep)>0;set(a(2), 'MarkerFaceColor', 'r');end
    xlim ([1 96])
    xlabel 'Epochs'
    ylabel 'Mean variance of channels'
    vline([8.5 48.5 88.5],{'k','k','k'},{'ME1/MI1','MI1/MI2','MI2/ME2'})
    legend ('Clean','Art')
    
    %{
    % global field power (GFP)
    gfp = std(mean(EEG.data,1));
    
    subplot(2,4,[7 8])
    plot(EEG.times, mean(EEG.data,1), 'k');
    hold on;
    plot(EEG.times, std(mean(EEG.data,1)), 'r', 'linewidth', 3);
    %}
    sgtitle(['zscore ERD topoplots for SUBJ: ' ERD(s).ID{:}]);

    save_fig(gcf,[PATHOUT_ERD 'plots\ind_data\'],['SUBJ_' FB{fb} '_' ERD(s).ID{:}],'fontsize',10);

end



end