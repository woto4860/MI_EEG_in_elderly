%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%              ERD for juw_el_table // All participants
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ERD Main
% Author: Julius Welzel // University of Oldenburg, 2019
% Version: 1.0 // 18.12.2019 // Initial setup

PATHIN_ICA = [MAIN '04_Data\ana02_clean\'];
PATHIN_EMG = [MAIN '04_Data\ana03_emg\'];
PATHOUT_ERD = [MAIN '04_Data\ana04_erd\'];

if ~exist(PATHOUT_ERD)
  mkdir(PATHOUT_ERD);
end

list = dir(fullfile([PATHIN_ICA '*clean*.set']));
SUBJ = extractBefore({list.name},'_');

load ([PATHIN_EMG 'EMG_ES.mat']); %includes cfg file
load ([PATHIN_EMG 'emg_all.mat']); %includes cfg file


%% Define params for ERD ME analyses to select ROI
cfg.ERD.HP = 8;
cfg.ERD.LP = 30;
cfg.ERD.EEG_ELECS = {'E01' 'E02' 'E03' 'E04' 'E05' 'E06' 'E07' 'E08' 'E09' 'E10' 'E11' 'E12' 'E13' 'E14' 'E15' 'E16' 'E17' 'E18' 'E19' 'E20' 'E21' 'E22' 'E23' 'E24' 'E25' 'E26' 'E27' 'E28' 'E29' 'E30' 'E31' 'E32' 'E33' 'E34' 'E35' 'E36' 'E37' 'E38' 'E39' 'E40' 'E41' 'E42' 'E43' 'E44' 'E45' 'E46' 'E47' 'E48' 'E49' 'E50' 'E51' 'E52' 'E53' 'E54' 'E55' 'E56' 'E57' 'E58' 'E59' 'E60' 'E61' 'E62' 'E63' 'E64'};
BL_ms = 2000;
cfg.ERD.BL = BL_ms/(1000/cfg.resample); %set 2 second baseline
BL_rERD_ms = [-2500:(1000/cfg.resample):-500];




%%
for sub = 1:length(SUBJ)

    if str2num(emg_check(sub).ID) == 85;continue;end
    
    % take different trials into account
    if str2num(emg_check(sub).ID)<50;    
        trials = [cfg.blck_idx{1}(1):cfg.blck_idx{3}(end) 249:256];
    else
        trials = [cfg.blck_idx{1}(1):cfg.blck_idx{4}(end)];
    end

    % load dataset  
    if str2num(SUBJ{sub})<50;    
        EEG = pop_loadset('filename',[SUBJ{sub},'_clean_ICA.set'],'filepath',PATHIN_ICA);
    else
        EEG = pop_loadset('filename',[SUBJ{sub},'_clean.set'],'filepath',PATHIN_ICA);
    end
    
    %checkset to re-order trigger via latency (different triggers are appended in LSL)
    EEG = eeg_checkset(EEG, 'eventconsistency' );
    %use 64 channels (omit EMG and others)
    EEG = pop_select( EEG,'channel',cfg.ERD.EEG_ELECS);
    %add chanlocs
    EEG = pop_chanedit(EEG, 'lookup',[MAIN '101_Software\' 'elec_64ch.elp']);
    %rename LSL-marker
    EEG = renameEvents_table(EEG);
    %rename redundant triggers
    EEG = triggerhand(EEG);
    %low-pass filter 30 Hz
    EEG = pop_firws(EEG, 'fcutoff', cfg.ERD.LP, 'ftype', 'lowpass', 'wtype', 'hamming', 'forder', 166);
    %resample to 250 Hz
    EEG = pop_resample(EEG, cfg.resample);
    %high-pass filter 1 Hz
    EEG = pop_firws(EEG, 'fcutoff', cfg.ERD.HP, 'ftype', 'highpass', 'wtype', 'hamming', 'forder', 414); 
    
    
    ERD(sub).ID = SUBJ(sub);
    
    %% Epoch data with power
    
    count=0;
    count_ep = 1;
    for h = 1:size(EEG.event,2)
        

            
        
        if strcmp(EEG.event(h).type, '1')
            
                begin_epoch = EEG.event(h).latency;
                flag = true;


        elseif strcmp(EEG.event(h).type, 'End') && flag == true
            
            count = count+1;
            %skip unneccessary epochs
            if count>trials(cfg.blck_idx{3}(end)) && count<trials(cfg.blck_idx{4}(1));continue;end      
            
            last_epoch = EEG.event(h).latency;
            bp_lat = cfg.EMG.thresh_BP(sub); %ES BP time in ms
            ERD(sub).filt_ep{count_ep}(:,:) = EEG.data(:,(begin_epoch-cfg.ERD.BL):last_epoch-(bp_lat/(1000/cfg.resample))).^2; % EEG data per trial // + BL // - BP time // square for ERD

            flag = false;
            count_ep = count_ep+1;

        end

    end
        
        

    
    %% relative ERD for all trials
    

        % calculate relative power for single trial

    for t = 1:length(trials)
        %single trial
        time_vec = [-BL_ms:(1000/EEG.srate):(length(ERD(sub).filt_ep{(t)})-cfg.ERD.BL)*(1000/EEG.srate)-1];
        ERD(sub).time_vec{t} = time_vec;
        trial_log = ~true(1, length(ERD(sub).filt_ep{(t)}));
        Base_log =  ismember(time_vec,BL_rERD_ms);
        Base_rERD_st = mean(ERD(sub).filt_ep{(t)}(:,Base_log),2); %Single trial baseline
        rERD{sub,t} = ((ERD(sub).filt_ep{(t)}-Base_rERD_st)./repmat(Base_rERD_st,1,length(ERD(sub).filt_ep{(t)})))*100;
        
        % average for ERD samller 0 for all trials
        real_ERD = rERD{sub,t}(:,:)<0; %after onset
        rERD{sub,t}(~real_ERD) = NaN;
        m_rERD(sub,:,t) = nanmean(rERD{sub,t}(:,time_vec>0),2);
        
        if isnan(EMG_all(sub,t)) | emg_check(sub).trials_valid(t) == 0
            m_rERD(sub,:,t) = NaN;
        end
    end % trial
     
end

cfg.ERD.time_vec = time_vec;
cfg.ERD.BL_ms = BL_ms;

%% Save data

save([PATHOUT_ERD 'ERD.mat'],'m_rERD','ERD');
save([PATHOUT_ERD 'cfg.mat'],'cfg');



