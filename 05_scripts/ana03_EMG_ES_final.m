%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%              EMG data analysis with Evolution strategy
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% EMG main
% Author: Mareike Daeglau // University of Oldenburg, 2018
% edited: 11.12.2018 // Julius Welzel

PATHIN = [MAIN '04_Data\ana03_emg\'];
PATHOUT_ES = [PATHIN 'emg_ES\'];


if ~exist(PATHOUT_ES)
  mkdir(PATHOUT_ES);
end

list = dir(fullfile([MAIN '04_Data\ana02_clean\' '*clean*.set']));
SUBJ = extractBefore({list.name},'_');

load ([PATHIN 'emg_all.mat']);

%reasoning and visualization go here: idea: check if movingstd exceeds std
%of trial of active hand (here only right) or resting hand (here only
%left). 
%case 1: movingstd exceeds std of trial in the active hand: reject trial
%case 2: movingstd exceeds std of trial in the resting hand but not in the
%active hand: sum both EMG streams and check again, if movingstd exceeds
%std of trial in the combined stream: reject, if not: keep

%note: you must know which channel is for which hand, because this is not
%the case here, we check all streams

load ([PATHIN 'cfg.mat']);

cfg.EMG.windN_movSD = 125;
cfg.EMG.thresh_BP = 900; 
win_size = cfg.EMG.windN_movSD;


%% mSet up ES params
%define variables
n_sub = size(emgdata,1);
n_trials = 96;

%ES params
t_emg = 1.5; % STD threshold EMG
t_BP_ms = 1000; % ms at beginning of ES
max_ffe = 50; % max calculations
mu = 2; 
lambda_ = 4;
i_sigma = 0.9;

%rechenberg
T = 5; %Rechenberg success
tau = 2;



% only interesting for the MI trials

for sub = 1:size(emgdata,1)

if isempty(emg_check(sub).trials_dur);continue;end;

%ES params

h_BP_fit = {};
h_INPUT = {};

ffe = 0;

pop_x = repmat([t_BP_ms t_emg],mu,1);
pop_fit = fit_EMG(emgdata,emg_check,pop_x(1,1),pop_x(1,2),cfg,sub);

h_BP_fit{end+1} = pop_fit(1,1);
h_INPUT{1,end+1} = pop_x(1,1);
h_INPUT{2,end} = pop_x(1,2);

%initiate Rechenberg
s = 0;
sigma = i_sigma; % stepsize


while ffe < max_ffe
    ffe = ffe+1;
    
    for j = 1:lambda_
        
        x_ = 0.5 * (pop_x(randi([1 mu],1,1),:)+pop_x(randi([1 mu],1,1),:));
        x_ = x_ + sigma * [rand_mut([-500 500],1) rand_mut([-1 1],1)];
        pop_x = [pop_x;x_];
        pop_fit_ = fit_EMG(emgdata,emg_check,x_(1),x_(2),cfg,sub);
        pop_fit = [pop_fit pop_fit_];
    end
    
    %Rechenberg
    if pop_fit_ > mean(pop_fit(1:2));
        s = s+1; %increase success counter
    end
        
    if mod(ffe,T) == 0
        if s/T > 1/5
            sigma = sigma * tau;
        elseif s/T < 1/5
            sigma = sigma/ tau;
        end

        s = 0;
    end
    
    if ffe >1
        % constraint for BP time
        if sum(pop_x(:,1) > 1600)>=1 
            i = pop_x(1:mu,1) > 1600;
            pop_x(i,1) = h_INPUT{1,end-1};
        elseif sum(pop_x(:,1) < 600)>=1
            i = pop_x(1:mu,1) < 600;
            pop_x(i,1) = h_INPUT{1,end-1};
        end
    end
    
    %save best offspring
    [out idx_fit] = sort(pop_fit,'descend');
    pop_fit = pop_fit(idx_fit);
    pop_fit(mu+1:end) = [];

    pop_x = pop_x(idx_fit,:);
    pop_x(mu+1:end,:) = []; %wipe out rest of generation
    
    %history
    h_BP_fit{end+1} = pop_fit(1,1);
    h_INPUT{1,end+1} = pop_x(1,1);
    h_INPUT{2,end} = pop_x(1,2);
    disp(['ES run:' num2str(ffe)])
    


    
end

% best output is stored
[cfg.EMG.acc(sub) EMG_all(sub,:)] = fit_EMG(emgdata,emg_check,pop_x(1,1),pop_x(1,2),cfg,sub);
cfg.EMG.thresh_STD(sub) = pop_x(1,2);
cfg.EMG.thresh_BP_ms(sub) = pop_x(1,1);
cfg.EMG.ID {sub} = emg_check.ID;


%% Plot success

M = figure;
left_color = [1 0 0];
right_color = [0 0 1];
set(M,'defaultAxesColorOrder',[left_color; right_color]);
%plot BP time
yyaxis left
plot(1:ffe+1,[h_INPUT{1,:}],'-s','color','r','MarkerFaceColor','red','MarkerSize',1)
ylim([300 1800])
ylabel 'BP time [ms]'
% plot EMG thresh
hold on
yyaxis right
plot(1:ffe+1,[h_INPUT{2,:}],'-s','color','b','MarkerFaceColor','blue','MarkerSize',1)
ylabel 'EMG threshold [STD]'

xlim([0 ffe])
xlabel 'ES runs'
title (['After ' num2str(ffe) ' runs // accuracy at: ' num2str(cfg.EMG.acc(sub))])
set(findall(gcf,'-property','FontSize'),'FontSize',18)
set(gcf, 'Position', [0 0 1200 800]);
saveas(gcf,[PATHOUT_ES 'EMG_ES_SUBJ_' SUBJ{sub},'.png']);
close;


end


%% Accuracy overview
RTs = [nanmean(EMG_all(:,cfg.blck_idx{1,:}),2) nanmean(EMG_all(:,cfg.blck_idx{2,:}),2) nanmean(EMG_all(:,cfg.blck_idx{3,:}),2) nanmean(EMG_all(:,cfg.blck_idx{4,:}),2)];
ACCs = 100*[NaN_acc(EMG_all(:,cfg.blck_idx{1,:})) NaN_acc(EMG_all(:,cfg.blck_idx{2,:})) NaN_acc(EMG_all(:,cfg.blck_idx{3,:})) NaN_acc(EMG_all(:,cfg.blck_idx{4,:}))];
ACC_MI = [NaN_acc(EMG_all(:,cfg.blck_idx{2,:}))*40 NaN_acc(EMG_all(:,cfg.blck_idx{3,:}))*40];

% Scatter
Ind = figure;
CT=cbrewer('qual', 'Paired', size(RTs,1));

for n = 1:size(RTs,1)
    j = -0.1 + (0.1+0.1)*rand(1,1);
    plot([1:size(RTs,2)]+j,ACCs(n,:),'-s','Color',CT(n,:),'MarkerFaceColor',CT(n,:), 'MarkerEdgeColor','none','MarkerSize',10)
    hold on
end

boxplot(40-ACC_MI',double(old))
ylabel 'Movement epochs [n]'
recolor_box(gcf,[c_young;c_old]);
xlabel 'Group'
xticks([1:2])
xticklabels({'Young','Old'})
pic_r = 1/1.7;
save_fig(gcf,PATHOUT_ES,['Movement_epoch_n'],'fontsize',15,'figsize',[0 0 22.7/2 pic_r*22.7/2],'figtype','.png');

% xlim([0 size(RTs,2)+1])
% ylim ([0 min(min(ACC_MI))+2])
% xlabel 'Blocks'
% xticks([1:2])
% xticklabels({'MI1','MI2'})
% title (['Clean epochs // acc: ' num2str(mean(ACCs,'all'))])
% set(findall(gcf,'-property','FontSize'),'FontSize',24)
% legend ([SUBJ])
% hline(70,'k')
% set(gcf, 'Position', [0 0 1400 800]);
% saveas(gcf,[PATHOUT_ES 'final_sigma_' num2str(i_sigma) '_tau_' num2str(tau) '_EMG_acc_overview.png']);
% close;

for b = 1:size(ACCs,2)
   score_EMG(1,b) = mean(ACCs(young,b)); 
   score_EMG(2,b) = mean(ACCs(old,b)); 
end

all_b_EMG = mean(score_EMG);
all_g_EMG = mean(score_EMG,2);


save([PATHIN 'EMG_ES.mat'],'EMG_all','cfg')

%% Validity plots EMG
% CAVE: Run first part of ana08_rela first


% Boxplot Threshold & BP time
BP_times = [cfg.EMG.thresh_BP_ms];
thresh_SD = [cfg.EMG.thresh_STD];

boxplot([BP_times],double(old))
recolor_box(gcf,[c_young;c_old]);
ylabel 'BP time [ms]'
xlabel 'Group'
xticks([1:2])
xticklabels({'Young','Old'})
pic_r = 1/1.7;
save_fig(gcf,PATHOUT_ES,['BP_times'],'fontsize',35,'figsize',[0 0 22.7 pic_r*22.7]);


boxplot([thresh_SD],double(old))
recolor_box(gcf,[c_young;c_old]);
ylabel 'Threshold [SD]'
xlabel 'Group'
xticks([1:2])
xticklabels({'Young','Old'})
save_fig(gcf,PATHOUT_ES,['Thresh_SD'],'fontsize',35,'figsize',[0 0 22.7 pic_r*22.7]);

[t_BP] = ttest2_param(BP_times(old),BP_times(young));
[t_thresh] = ttest2_param(thresh_SD(old),thresh_SD(young));

%% Find EMG movement in MI

[r c] = find(isnan(EMG_all(:,MI_trials)));

% for i = 1:length(r)
%     figure
%     plot(emgdata{r(i),c(i)}(1,:))
% end
% 
figure
subplot(2,1,1)
ep=13;
sub=11;
    t_vec = linspace(0,length(emgdata{sub,ep})*(1000/cfg.EMG.srate),length(emgdata{sub,ep}));
    plot(t_vec,emgdata{sub,ep}(1,:),'Color',[0.7 0.7 0.7])
%     ylabel 'Amplitude [\muV]'
%     xlabel 'Time [ms]'
    axis tight
    v = vline(length(emgdata{sub,ep})*(1000/cfg.EMG.srate)-cfg.EMG.thresh_BP_ms(sub)/1.8,'-k')
    v.LineWidth = 2;

subplot(2,1,2)
    t_vec = linspace(0,length(emgdata{11,55})*(1000/cfg.EMG.srate),length(emgdata{11,55}));
    plot(t_vec,emgdata{11,55}(1,:),'Color',[0.7 0.7 0.7])
    ylabel '                Amplitude [\muV]'
    xlabel 'Time [ms]'
    axis tight
    v = vline(length(emgdata{11,55})*(1000/cfg.EMG.srate)-cfg.EMG.thresh_BP_ms(sub)/1.8,'-k');
    v.LineWidth = 2;

pic_r = 1/1.7;
save_fig(gcf,PATHOUT_ES,['BP_emg_eg'],'fontsize',15,'figsize',[0 0 22.7/2 pic_r*22.7/2])

