%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%                  Clean data with ICA weights
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Data: juw_el_table (Mareike Daeglau, University of Oldenburg)
% Author: Julius Welzel

% define paths
PATHIN_RAW = [MAIN '04_Data\raw_data\'];
PATHIN_ICA = [MAIN '04_Data\ana01_ICAw\'];
PATHOUT_CLEAN = [MAIN '04_Data\ana02_clean\'];
PATHOUT_CLEAN_PLOT = [PATHOUT_CLEAN 'ica_plots\'];

if ~exist(PATHOUT_CLEAN)
  mkdir(PATHOUT_CLEAN);
  mkdir(PATHOUT_CLEAN_PLOT);
end

% document potential problems with a subject
if exist([PATHOUT_CLEAN 'docError.mat']) == 2
    load([PATHOUT_CLEAN 'docError.mat']);
else
    docError = {};      
end

%call eeglab
[ALLEEG, EEG, CURRENTSET, ALLCOM] = eeglab;

%% SET PARAMETERS

load ([PATHIN_ICA 'cfg.mat']);

%% FIND DATASETS

% find all Datasets
list = dir(fullfile([PATHIN_ICA '*ICAw.set']));
SUBJ = extractBefore({list.name},'_');

%% Reject ICA Components
for sub = 1:length(SUBJ)
    if exist([PATHOUT_CLEAN,SUBJ{sub},'_clean.set'],'file') == 2
        disp(['ICA for ' SUBJ{sub} ' has already been cleaned. Continue with next dataset.']);
        pause(0.2);
    else
    try
    
    %% load dataset with ICA weights
    EEG = pop_loadset('filename',[SUBJ{sub} '_mergeICAw.set'],'filepath',PATHIN_ICA);
    [ALLEEG, EEG, CURRENTSET] = eeg_store( ALLEEG, EEG, 0 );
    EEG = eeg_checkset( EEG );

    %% open all for selection relevant figures
    figure;
    [pic,I] = imread([path_data,'ana00_sanity\indv_plots\',SUBJ{sub},'_ICA_eye.png']);
    imshow(pic,I);
    pop_eegplot( EEG, 0, 1, 0);
    pop_selectcomps(EEG, [1:35] );

    %% enter bad components
    badcomps = [];
    badcomps = input(['Enter bad components indices [], separate with comma for SUBJ' SUBJ{sub} ': ']);
%     badcomps = horzcat (badcomps,EEG.ana04ica.eyecatching);
    badcomps = unique(sort(badcomps));
    close all
    %% store bad components & ICA relevant info in tmp
    TMP.icaweights = EEG.icaweights;
    TMP.icawinv = EEG.icawinv;
    TMP.icasphere = EEG.icasphere;
    TMP.badcomps = badcomps;
    TMP.icachansind = EEG.icachansind;
    
      %% save topoplot with name of bad components included
    pop_topoplot(EEG,0, [1:35] ,['Bad components for rejection selected: ' num2str(TMP.badcomps)],[6 6] ,0,'electrodes','on');
    print([PATHOUT_CLEAN_PLOT , SUBJ{sub},'_ICA_topop_bc.png'],'-dpng');  % raster image
    close
       
    %% save specs of only bad components
    pop_prop( EEG, 0, badcomps , NaN, {'freqrange' [1 40] });    
    figs = findobj(0, 'type', 'figure');
    badcomps = fliplr(badcomps);

    for f = 1:length(figs)
        print(figs(f),[PATHOUT_CLEAN_PLOT, SUBJ{sub},'_badcomp_',num2str(badcomps(f)),'.png'],'-dpng');  % raster image
    end
    close all
    
    %% Load "original" data set
    if sub == 5
        %data set 40 had to be merged beforehand so it is already a set file
        EEG = pop_loadset('filename',[SUBJ{sub} '_MI_corrected.set'],'filepath',PATHIN_RAW);
    else
        %load xdf files
        EEG = pop_loadxdf([PATHIN_RAW SUBJ{sub} '_MI.xdf'], 'streamtype', 'EEG', 'exclude_markerstreams', {});
    end

    %add chanlocs
    EEG = pop_chanedit(EEG, 'lookup',[MAIN '101_software\elec_64ch.elp']);

    %% overwrite tmp ICA info in original data
    EEG.icaweights = TMP.icaweights;
    EEG.icawinv = TMP.icawinv;
    EEG.icasphere = TMP.icasphere;
    EEG.badcomps = TMP.badcomps;
    EEG.icachansind = TMP.icachansind;
    
    %% delete bad components
    EEG = pop_subcomp(EEG, [EEG.badcomps], 0);
    EEG = eeg_checkset(EEG);
    EEG = pop_saveset( EEG, 'filename',[SUBJ{sub},'_clean.set'],'filepath',PATHOUT_CLEAN);
    
   catch % write in Error doc
        disp(['Error with ' SUBJ{sub}])
        docError(end+1) = SUBJ(sub);
        close
        continue;
    end
    end
end
save([PATHOUT_CLEAN 'docError'], 'docError');


