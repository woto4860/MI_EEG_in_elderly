# Exploring time – Motor imagery EEG Features in elderly
A master thesis by Julius Welzel at the University of Oldenburg

## Author
Developed by Julius Welzel, University of Oldenburg <br>
Supervised by Mareike Daeglau & Cornelia Kranczioch, University of Oldenburg 

## Versioning
<i>Version 1.0 // 27.11.18 </i> [Initial Setup] <br>
<i>Version 1.1 // 17.01.19 </i> [Thesis added & preprocessing complete]<br>
<i>Version 1.2 // 29.01.19 </i> [Final analysis scripts included]<br>
<i>Version 1.3 // 07.02.19 </i> [Preprocessing revised]<br>
<i>Version 2.0 // 06.04.19 </i> [Final results done]<br>